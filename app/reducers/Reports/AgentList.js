import {
  AGENT_LIST_REQUEST,
  AGENT_LIST_SUCCESS,
  AGENT_LIST_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  agentList: [],
  metaAgentList: {
    total_count: 0,
    current: 0,
  },
}

export default function agentList(state = initialState, action) {
  switch (action.type) {
    case AGENT_LIST_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case AGENT_LIST_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        agentList: action.data,
        metaAgentList: action.meta,
      }
    case AGENT_LIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    default:
      return state
  }
}
