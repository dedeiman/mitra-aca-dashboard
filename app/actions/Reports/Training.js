/* eslint-disable camelcase */
import API from 'utils/API'
import {
  REPORT_TRAINING_REQUEST,
  REPORT_TRAINING_SUCCESS,
  REPORT_TRAINING_FAILURE,
  REPORT_TRAINING_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

export const TrainingRequest = () => ({
  type: REPORT_TRAINING_REQUEST,
})

export const TrainingSuccess = (data, meta) => ({
  type: REPORT_TRAINING_SUCCESS,
  data,
  meta,
})

export const TrainingFailure = error => ({
  type: REPORT_TRAINING_FAILURE,
  error,
})

export const TrainingDetailSuccess = (data, meta) => ({
  type: REPORT_TRAINING_DETAIL_SUCCESS,
  data,
  meta,
})

export const fetchTraining = ({
  page = 1,
  subject_id = '',
  training_id = '',
  branch_organizer_id = '',
  is_detail = false,
  from_date = '',
  to_date = '',
  per_page = 10,
} = {}) => (
  (dispatch) => {
    dispatch(TrainingRequest())
    Helper.sessionTimeout()
    return API.get(`/report/training-class?page=${page}&per_page=${per_page}&subject_id=${subject_id}&branch_organizer_id=${branch_organizer_id}&training_id=${training_id}&is_detail=${is_detail}&from_date=${from_date}&to_date=${to_date}`)
      .then((res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(TrainingSuccess(data, meta))
        } else {
          dispatch(TrainingFailure(meta.message))
        }
      }).catch((err) => {
        dispatch(TrainingFailure(err.message))
      })
  }
)
