/* eslint-disable camelcase */
import API from 'utils/API'
import {
  AGENT_LIST_REQUEST,
  AGENT_LIST_SUCCESS,
  AGENT_LIST_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../../utils/Helper'

export const agentListRequest = () => ({
  type: AGENT_LIST_REQUEST,
})

export const agentListSuccess = (data, meta) => ({
  type: AGENT_LIST_SUCCESS,
  data,
  meta,
})

export const agentListFailure = error => ({
  type: AGENT_LIST_FAILURE,
  error,
})

export const fetchAgentList = params => (
  (dispatch) => {
    dispatch(agentListRequest())
    Helper.sessionTimeout()
    return API.get(`reports/agents?${params}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(agentListSuccess(data, meta))
        } else {
          dispatch(agentListFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(agentListFailure(err.message))
    })
  }
)
