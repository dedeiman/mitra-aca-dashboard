import Cookies from 'js-cookie'
import { isEmpty } from 'lodash'
import { purgeStoredState } from 'redux-persist'
import { mainPersistConfig } from 'store/configureStore'
import history from 'utils/history'
import config from 'app/config'
import Browser from 'utils/Browser'
import Swal from 'sweetalert2'
import API from 'utils/API'
import {
  AUTHENTICATE_USER_REQUEST,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_FAILURE,
  UPDATE_AUTH_CURRENT_USER,
} from 'constants/ActionTypes'
import { updateSiteConfiguration } from './Site'
import Helper from '../utils/Helper'


export const authenticateUserRequest = () => ({
  type: AUTHENTICATE_USER_REQUEST,
})

export const authenticateUserSuccess = data => ({
  type: AUTHENTICATE_USER_SUCCESS,
  currentUser: data,
})

export const authenticateUserFailure = errorMessage => ({
  type: AUTHENTICATE_USER_FAILURE,
  errorMessage,
})

export const updateAuthCurrentUser = currentUser => ({
  type: UPDATE_AUTH_CURRENT_USER,
  currentUser,
})

export const removeToken = () => {
  Cookies.remove(config.auth_cookie_name, { path: '/' })
}

export const redirectToLogin = () => (
  Browser.setWindowHref('/')
)

export const getAccessToken = () => (
  Cookies.get(config.auth_cookie_name)
)

export const loginUser = (data, url) => (
  (dispatch) => {
    dispatch(authenticateUserSuccess(data))
    dispatch(updateSiteConfiguration('activePage', 'dashboard'))

    Cookies.set(config.auth_cookie_name, data.auth_token, {
      path: '/',
      // domain: Browser.getRootDomain(),
    })

    history.push(url || '/dashboard')
  }
)

export const authenticateByCredentials = params => (
  (dispatch) => {
    dispatch(authenticateUserRequest())
    return API.post('/auth/login', params, { headers: { 'x-api-key': config.api_key } }).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          if (data.is_first_login === true) {
            localStorage.setItem('isFirstLogin', true)
            dispatch(loginUser(data, '/my-password/#first'))
          } else {
            dispatch(loginUser(data))
          }
        } else {
          dispatch(updateAuthCurrentUser(null))
          dispatch(authenticateUserFailure(response.data.meta.message))
        }
      },
    ).catch((err) => {
      if (err.response && err.response.data) {
        dispatch(authenticateUserFailure(err.response.data.meta.message)) // eslint-disable-line no-console
      } else {
        dispatch(authenticateUserFailure(err.message)) // eslint-disable-line no-console
      }
    })
  }
)

export const authenticateByToken = () => (
  (dispatch) => {
    const accessToken = getAccessToken()

    if (isEmpty(accessToken)) {
      return redirectToLogin()
    }

    dispatch(authenticateUserRequest())
    Helper.sessionTimeout()

    return API.post(
      '/auth/authorization',
      { token: accessToken },
    ).then(
      (response) => {
        if (response.data.status) {
          dispatch(authenticateUserSuccess({ access_token: accessToken }))
        } else {
          dispatch(authenticateUserFailure())
          redirectToLogin()
        }
      },
    ).catch((err) => {
      console.error(err) // eslint-disable-line no-console
    })
  }
)

export const otpPassword = identifier => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        '/passwords/reset/verify-otp',
        identifier,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
          Swal.fire(
            meta.message,
            '',
            'success',
          ).then(() => {
            if (data.is_otp_verification) {
              history.push('/password/otp')
            } else {
              history.push('/')
            }
          })
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)

export const forgotPassword = identifier => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        'passwords/reset',
        identifier,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          if (!data.is_otp_verification) {
            Swal.fire(
              'Berhasil Mereset Password!',
              'Silakan cek email Kamu',
              'success',
            ).then(() => {
              if (data.is_otp_verification) {
                history.push('/password/otp')
              } else {
                history.push('/')
              }
            })
          } else {
            resolve(data)
          }
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)

export const resetPassword = (payload, isLoggedIn) => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        'passwords/change',
        payload,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
          Swal.fire(
            'Password has been change!',
            '',
            'success',
          ).then(() => {
            if (isLoggedIn) {
              purgeStoredState(mainPersistConfig).then(() => {
                removeToken()
                Browser.setWindowHref('/')
              })
            } else {
              history.push('/')
            }
          })
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)

export const clearCurrentUser = () => (
  () => {
    purgeStoredState(mainPersistConfig).then(() => {
      removeToken()
      Browser.setWindowHref('/')
    })
  }
)

export const verifyPassword = payload => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        'passwords/verify',
        payload,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)
