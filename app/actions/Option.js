import API from 'utils/API'
import { message } from 'antd'
import config from 'app/config'

const base = {
  apiUser: config.api_url,
  apiContent: config.api_url_content,
  apiPayment: config.api_url_payment,
}

export const getDatas = (settings = {}, payload = {}) => (
  () => (
    new Promise((resolve, reject) => {
      const url = base[settings.base] + settings.url

      return API[settings.method](
        url,
        payload,
      ).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve({ data, meta })
          } else {
            reject(meta.message)
          }
        },
      ).catch((err) => {
        reject(err)
      })
    })
  )
)

export const getCOB = () => (
  () => new Promise(resolve => (
    API.get('/class-of-business').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getBranches = key => (
  () => new Promise(resolve => (
    API.get(`/branch${key || 'es'}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getRepresentativeBranch = key => (
  () => new Promise(resolve => (
    API.get(`/branch-perwakilan/${key || ''}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getProducts = key => (
  () => new Promise(resolve => (
    API.get(`/product${key || 's?purpose=select'}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getTraining = () => (
  () => new Promise(resolve => (
    API.get('/training_classes').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getSubject = () => (
  () => new Promise(resolve => (
    API.get('/training-subjects').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getContest = () => (
  () => new Promise(resolve => (
    API.get('/contests').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getLevel = () => (
  () => new Promise(resolve => (
    API.get('/levels').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getStatus = () => (
  () => new Promise(resolve => (
    API.get('/status').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)

export const getAgentStatus = () => (
  () => new Promise(resolve => (
    API.get('/agent-change-reasons').then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)
