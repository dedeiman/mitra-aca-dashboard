import React from 'react'
import { Spin, Icon } from 'antd'

const antIcon = <Icon type="loading" style={{ fontSize: 34 }} spin />

const Loader = () => (
  <div className="wrapper-loader">
    <Spin indicator={antIcon} className="mb-4" />
    Getting things ready...
  </div>
)

export default Loader
