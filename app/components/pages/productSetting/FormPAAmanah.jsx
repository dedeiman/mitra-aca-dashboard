/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormPAAmanah = ({
  detailProduct, handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields, getFieldValue } = form
  const ub1 = Object.assign({}, stateLiability.list.A)
  const ub2 = Object.assign({}, stateLiability.list.B)
  const ub3 = Object.assign({}, stateLiability.list.C)
  return (
    <Card title="Edit Product Setting - PA Amanah">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Row gutter={24}>
          <Col span={6}>
            <label>Class</label>
          </Col>
          <Col span={6}>
            <label>Rate (%)</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="ml-2">{Object.assign({}, ub1).class}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu1}>
              {getFieldDecorator('bu1', {
                initialValue: Object.assign({}, ub1).rate_percentage || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="ml-2">{Object.assign({}, ub2).class}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu2}>
              {getFieldDecorator('bu2', {
                initialValue: Object.assign({}, ub2).rate_percentage || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="ml-2">{Object.assign({}, ub3).class}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu3}>
              {getFieldDecorator('bu3', {
                initialValue: Object.assign({}, ub3).rate_percentage || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormPAAmanah.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormPAAmanah)
