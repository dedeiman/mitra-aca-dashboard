/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'
import NumberFormat from 'react-number-format'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormLiability = ({
  detailProduct, handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields, getFieldValue } = form

  return (
    <Card title="Edit Product Setting - Liability">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item label="Premi" {...errorsField.premium_amount}>
              {getFieldDecorator('premium_amount', {
                initialValue: stateLiability.list.premium_amount,
                rules: [
                  ...Helper.fieldRules(['required'], 'premium_amount'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Nilai Pertanggungan" {...errorsField.coverage_amount}>
              {getFieldDecorator('coverage_amount', {
                initialValue: stateLiability.list.coverage_amount,
                rules: [
                  ...Helper.fieldRules(['required'], 'coverage_amount'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormLiability.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormLiability)
