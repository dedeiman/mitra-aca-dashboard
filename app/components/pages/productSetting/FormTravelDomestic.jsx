/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import NumberFormat from 'react-number-format'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormTravelDomestic = ({
  detailProduct, handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields, getFieldValue } = form
  const dataTravelDomestic = Object.assign({}, stateLiability.list.travel_domestic)
  const ub1 = Object.assign({}, dataTravelDomestic['1-4'])
  const ub2 = Object.assign({}, dataTravelDomestic['5-11'])
  const ub3 = Object.assign({}, dataTravelDomestic['12-20'])
  const ub4 = Object.assign({}, dataTravelDomestic['21-31'])
  const ub5 = Object.assign({}, dataTravelDomestic.additional_week)
  return (
    <Card title="Edit Product Setting - Travel Domestic">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Row gutter={24}>
          <Col span={6}>
            <label>Lama Perjalanan</label>
          </Col>
          <Col span={6}>
            <label>Nusantara 1</label>
          </Col>
          <Col span={6}>
            <label>Nusantara 2</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="mt-1"> 1 - 4</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu11}>
              {getFieldDecorator('bu11', {
                initialValue: Object.assign({}, ub1[0]).nusantara_1 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                <NumberFormat
                  autoComplete="off"
                  className="ant-input field-lg"
                  thousandSeparator
                  prefix=""
                />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu12}>
              {getFieldDecorator('bu12', {
                initialValue: Object.assign({}, ub1[0]).nusantara_2 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="mt-1"> 5 - 11</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu21}>
              {getFieldDecorator('bu21', {
                initialValue: Object.assign({}, ub2[0]).nusantara_1 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu22}>
              {getFieldDecorator('bu22', {
                initialValue: Object.assign({}, ub2[0]).nusantara_2 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="mt-1"> 12 - 20</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu31}>
              {getFieldDecorator('bu31', {
                initialValue: Object.assign({}, ub3[0]).nusantara_1 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu32}>
              {getFieldDecorator('bu32', {
                initialValue: Object.assign({}, ub3[0]).nusantara_2 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="mt-1"> 21 - 31</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu41}>
              {getFieldDecorator('bu41', {
                initialValue: Object.assign({}, ub4[0]).nusantara_1 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu42}>
              {getFieldDecorator('bu42', {
                initialValue: Object.assign({}, ub4[0]).nusantara_2 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label className="mt-1"> Tambahan Per Minggu</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu51}>
              {getFieldDecorator('bu51', {
                initialValue: Object.assign({}, ub5[0]).nusantara_1 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu51'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu52}>
              {getFieldDecorator('bu52', {
                initialValue: Object.assign({}, ub5[0]).nusantara_2 || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu52'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormTravelDomestic.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormTravelDomestic)
