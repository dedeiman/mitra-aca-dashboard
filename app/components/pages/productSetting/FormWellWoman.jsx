/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'
import NumberFormat from 'react-number-format'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormWellWoman = ({
  detailProduct, handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields, getFieldValue } = form
  const ub1 = Object.assign({}, stateLiability.list['18-40'])
  const ub2 = Object.assign({}, stateLiability.list['41-55'])
  const ub3 = Object.assign({}, stateLiability.list['56-64'])
  return (
    <Card title="Edit Product Setting - WellWoman">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Row gutter={24}>
          <Col span={6}>
            <label>Batasan Usia</label>
          </Col>
          <Col span={6}>
            <label>Plan1</label>
          </Col>
          <Col span={6}>
            <label>Plan2</label>
          </Col>
          <Col span={6}>
            <label>Plan3</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label>18-40</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu1plan1}>
              {getFieldDecorator('bu1plan1', {
                initialValue: Object.assign({}, ub1[0]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu1plan1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu1plan2}>
              {getFieldDecorator('bu1plan2', {
                initialValue: Object.assign({}, ub1[1]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu1plan2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu1plan3}>
              {getFieldDecorator('bu1plan3', {
                initialValue: Object.assign({}, ub1[2]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu1plan3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label>41-55</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu2plan1}>
              {getFieldDecorator('bu2plan1', {
                initialValue: Object.assign({}, ub2[0]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu2plan1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu2plan2}>
              {getFieldDecorator('bu2plan2', {
                initialValue: Object.assign({}, ub2[1]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu2plan2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu2plan3}>
              {getFieldDecorator('bu2plan3', {
                initialValue: Object.assign({}, ub2[2]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu2plan3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={6}>
            <label>56-64</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu3plan1}>
              {getFieldDecorator('bu3plan1', {
                initialValue: Object.assign({}, ub3[0]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu3plan1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu3plan2}>
              {getFieldDecorator('bu3plan2', {
                initialValue: Object.assign({}, ub3[1]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu3plan2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bu3plan3}>
              {getFieldDecorator('bu3plan3', {
                initialValue: Object.assign({}, ub3[0]).premium_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bu3plan3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormWellWoman.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormWellWoman)
