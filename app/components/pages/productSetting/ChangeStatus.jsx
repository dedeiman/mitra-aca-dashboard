/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Select, DatePicker,
  Descriptions, Button,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'
import moment from 'moment'

const ChangeStatus = ({
  toggle, form, onSubmit,
  detailCustomer, stateForm,
  loadSubmitBtn,
  getDownline,
}) => {
  const { getFieldDecorator } = form
  return (
    <Form onSubmit={onSubmit}>
      <Row gutter={24}>
        <Col span={24}>
          <Descriptions layout="vertical" colon={false} column={3}>
            <Descriptions.Item label="Customer Number" className="px-1 profile-detail pb-0">
              {detailCustomer.customer_number || '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Customer Type" className="px-1 profile-detail pb-0">
              {(detailCustomer.customer_type || '-').toUpperCase()}
            </Descriptions.Item>
            <Descriptions.Item label="Customer Name" className="px-1 profile-detail pb-0">
              {detailCustomer.name || '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Current Status" className="px-1 profile-detail pb-0">
              <Button
                className="text-white text-capitalize rounded-pill"
                style={{ backgroundColor: '#51b72b', width: '100%' }}
              >
                {(detailCustomer.status_active || 'Non Verification').toUpperCase()}
              </Button>
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={12}>
          <p className="mb-0">Tanggal Efektif</p>
          <Form.Item>
            {getFieldDecorator('effective_date', {
              rules: Helper.fieldRules(['required']),
              initialValue: moment(),
            })(
              <DatePicker
                disabled
                size="large"
                className="w-100"
                format="DD MMMM YYYY"
                disabledDate={current => (current && (current < moment().subtract(1, 'day')))}
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={12}>
          <p className="mb-0">Change Status to</p>
          <Form.Item>
            {getFieldDecorator('status_active', {
              rules: Helper.fieldRules(['required']),
              initialValue: detailCustomer.status_active,
              getValueFromEvent: (val) => {
                if (val.includes('restricted')) {
                  getDownline(detailCustomer.agent_id)
                }
                return val
              },
            })(
              <Select
                size="large"
                className="w-100"
                placeholder="Select Status"
                loading={stateForm.statusLoad}
              >
                {(stateForm.statusList || []).map(item => (
                  <Select.Option key={item.id} value={item.slug}>{item.display_name}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            className="mr-2"
            disabled={loadSubmitBtn}
          >
            Submit
          </Button>
          <Button
            ghost
            size="large"
            type="primary"
            onClick={() => {
              form.resetFields()
              toggle()
            }}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

ChangeStatus.propTypes = {
  detailCustomer: PropTypes.object,
  toggle: PropTypes.func,
  form: PropTypes.any,
  stateForm: PropTypes.object,
  onSubmit: PropTypes.func,
  getDownline: PropTypes.func,
  loadSubmitBtn: PropTypes.bool,
}

export default ChangeStatus
