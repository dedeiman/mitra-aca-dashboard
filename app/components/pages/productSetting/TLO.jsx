import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Table, Collapse,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse

function callback(key) {
  console.log(key)
}

const columns = [
  {
    title: 'Uang Pertanggungan',
    dataIndex: 'jaminan',
    key: 'jaminan',
    render: (text, row, index) => {
      if (index > 0) {
        return <a>{text}</a>;
      }
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: 1,
        },
      };
    },
  },
  {
    title: 'TLO',
    children: [
      {
        title: 'Wilayah 1 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb1',
            key: 'bb1',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt1',
            key: 'bt1',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba1',
            key: 'ba1',
          },
        ],
      },
      {
        title: 'Wilayah 2 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb2',
            key: 'bb2',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt2',
            key: 'bt2',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba2',
            key: 'ba2',
          },
        ],
      },
      {
        title: 'Wilayah 3 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb3',
            key: 'bb3',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt3',
            key: 'bt3',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba3',
            key: 'ba3',
          },
        ],
      },
    ],
  },
]

const columnsBencana = [
      {
        title: '',
        dataIndex: 'bencanaAlam',
        key: 'bencanaAlam',
      },
      {
        title: 'Wilayah 1 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb1',
            key: 'bb1',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt1',
            key: 'bt1',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba1',
            key: 'ba1',
          },
        ],
      },
      {
        title: 'Wilayah 2 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb2',
            key: 'bb2',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt2',
            key: 'bt2',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba2',
            key: 'ba2',
          },
        ],
      },
      {
        title: 'Wilayah 3 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb3',
            key: 'bb3',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt3',
            key: 'bt3',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba3',
            key: 'ba3',
          },
        ],
      },
]

const columnsTPL = [
  {
    title: 'TPL',
    dataIndex: 'tpl',
    key: 'tpl',
    width: 500,
  },
  {
    title: 'TPL 50jt jumlah premi minimum 375.000',
    dataIndex: 'tpl1',
    key: 'tpl1',
  },
]

const columnsPA = [
  {
    title: 'PA Driver',
    dataIndex: 'pa_driver',
    key: 'pa_driver',
  },
  {
    title: 'PA Passenger',
    dataIndex: 'pa_passenger',
    key: 'pa_passenger',
  },
]

const TLOSetting = ({
  stateLiability, dataCustomer,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const ub1 = Object.assign({}, stateLiability.list.comprehensive_tlo_premi_rates)
  const ub2 = Object.assign({}, stateLiability.list.comprehensive_tlo_natural_disaster_rates)
  const ub3 = Object.assign({}, stateLiability.list.third_party_liability_rates)
  const ub4 = Object.assign({}, stateLiability.list.personal_accident_rates)
  const r1 = Object.assign({}, ub1['>100jt-125jt'])
  const r2 = Object.assign({}, ub1['>125jt-200jt'])
  const r3 = Object.assign({}, ub1['>200jt-400jt'])
  const r4 = Object.assign({}, ub1['>400jt-800jt'])
  const r5 = Object.assign({}, ub1['>800jt'])
  const b1 = Object.assign({}, ub2.eq)
  const b2 = Object.assign({}, ub2.flood)
  const b3 = Object.assign({}, ub2.rscc)
  const b4 = Object.assign({}, ub2.ts)

  const dataPA = [
    {
      key: '1',
      pa_driver: Object.assign({}, Object.assign({}, ub4['pad'])).rate,
      pa_passenger: Object.assign({}, Object.assign({}, ub4['pap'])).rate,
    },
  ]

  const data = [
    {
      key: '1',
      jaminan: '> 100 s/d 125 juta',
      bb1: Object.assign({}, Object.assign({}, r1['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r1['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r1['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r1['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r1['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r1['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r1['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r1['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r1['area_3'])[2]).rate,
    },
    {
      key: '2',
      jaminan: '> 125 s/d 200 juta',
      bb1: Object.assign({}, Object.assign({}, r2['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r2['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r2['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r2['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r2['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r2['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r2['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r2['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r2['area_3'])[2]).rate,
    },
    {
      key: '3',
      jaminan: '> 200 s/d 400 juta',
      bb1: Object.assign({}, Object.assign({}, r3['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r3['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r3['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r3['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r3['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r3['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r3['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r3['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r3['area_3'])[2]).rate,
    },
    {
      key: '4',
      jaminan: '> 400 s/d 800 juta',
      bb1: Object.assign({}, Object.assign({}, r4['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r4['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r4['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r4['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r4['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r4['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r4['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r4['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r4['area_3'])[2]).rate,
    },
    {
      key: '5',
      jaminan: '> 800 juta',
      bb1: Object.assign({}, Object.assign({}, r5['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r5['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r5['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r5['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r5['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r5['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r5['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r5['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r5['area_3'])[2]).rate,
    },
  ]
  const dataBencana = [
    {
      key: '1',
      bencanaAlam: 'Angin Topan, Badai, Hujan Es, Banjir dan atau Tanah Longsor',
      bb1: Object.assign({}, Object.assign({}, b2['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b2['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b2['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b2['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b2['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b2['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b2['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b2['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b2['area_3'])[2]).rate,
    },
    {
      key: '2',
      bencanaAlam: 'Gempa Bumi, Tsunami, dan atau Letusan Gunung Berapi',
      bb1: Object.assign({}, Object.assign({}, b1['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b1['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b1['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b1['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b1['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b1['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b1['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b1['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b1['area_3'])[2]).rate,
    },
    {
      key: '3',
      bencanaAlam: 'Huru-hara dan Kerusuhan (SRCC)',
      bb1: Object.assign({}, Object.assign({}, b3['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b3['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b3['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b3['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b3['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b3['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b3['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b3['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b3['area_3'])[2]).rate,
    },
    {
      key: '4',
      bencanaAlam: 'Terorisme dan Sabotase',
      bb1: Object.assign({}, Object.assign({}, b4['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b4['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b4['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b4['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b4['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b4['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b4['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b4['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b4['area_3'])[2]).rate,
    },
  ]
  const dataTPL = [
    {
      key: '1',
      tpl: Object.assign({}, ub3[0]).max_price,
      tpl1: Object.assign({}, ub3[0]).rate,
    },
    {
      key: '2',
      tpl: Object.assign({}, ub3[1]).max_price,
      tpl1: Object.assign({}, ub3[1]).rate,
    },
    {
      key: '3',
      tpl: Object.assign({}, ub3[2]).max_price,
      tpl1: Object.assign({}, ub3[2]).rate,
    },
  ]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <div className="title-page">Product - TLO</div>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="button-lg px-4">
              Edit TLO
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataCustomer)) && (
            <>
              <Table columns={columns} dataSource={data} bordered size="small" pagination={false} className="mb-4" />
              <Collapse defaultActiveKey={['1']} accordion="true" onChange={callback} expandIconPosition="right">
                <Panel header="Bencana Alam" key="1">
                  <Table columns={columnsBencana} dataSource={dataBencana} pagination={false} bordered size="large" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['2']} onChange={callback} className="mt-4" expandIconPosition="right">
                <Panel header="TPL" key="2">
                  <Table columns={columnsTPL} dataSource={dataTPL} bordered pagination={false} size="small" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['3']} onChange={callback} className="mt-4" expandIconPosition="right">
                <Panel header="PA" key="3">
                  <Table columns={columnsPA} dataSource={dataPA} pagination={false} bordered size="small" />
                </Panel>
              </Collapse>
            </>
        )}
        {!isEmpty(dataCustomer)
          ? (
            <>
              <Table columns={columns} dataSource={data} bordered size="small" pagination={false} className="mb-4" />
              <Collapse defaultActiveKey={['1']} accordion="true" onChange={callback} expandIconPosition="right">
                <Panel header="Bencana Alam" key="1">
                  <Table columns={columnsBencana} dataSource={dataBencana} pagination={false} bordered size="large" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['2']} onChange={callback} className="mt-4" expandIconPosition="right">
                <Panel header="Bencana Alam" key="2">
                  <Table columns={columnsBencana} dataSource={dataBencana} pagination={false} bordered size="small" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['3']} onChange={callback} className="mt-4" expandIconPosition="right">
                <Panel header="PA" key="4">
                  <Table columns={columnsPA} dataSource={dataPA} pagination={false} bordered size="small" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['4']} onChange={callback} className="mt-4" expandIconPosition="right">
                <Panel header="TPL" key="3">
                  <Table columns={columnsTPL} dataSource={dataTPL} bordered pagination={false} size="small" />
                </Panel>
              </Collapse>
            </>
          )
          : <div className="py-5" />
          }
      </Card>
    </React.Fragment>
  )
}

TLOSetting.propTypes = {
  stateLiability: PropTypes.object,
  dataCustomer: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default TLOSetting
