import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input, Button,
  Alert, Checkbox, Row, Col,
} from 'antd'
import { capitalize } from 'lodash'
import { Loader } from 'components/elements'

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 5,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 12,
    },
  },
}

const FormRoleGroup = ({
  currentData,
  handleSubmit, form, checkedList,
  isEdit, isFetching, errorMessage,
  onChange, errorsField, setErrorsField, statePermission,
}) => {
  const { getFieldDecorator } = form

  return (
    <Card title={`Form ${isEdit ? 'Edit' : 'Add'} Role Group`}>
      {isFetching && <Loader />}
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          className="mb-3"
        />
      )}
      <Form {...formItemLayout} onSubmit={handleSubmit}>
        <Form.Item label="Code" {...errorsField.code}>
          {getFieldDecorator('code', {
            initialValue: currentData.code || '',
            rules: [
              { pattern: new RegExp('^[a-zA-Z0-9]{1,20}$'), message: 'Cannot input with special character and longer than 20 character' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                code: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Auto Generate" disabled />,
          )}
        </Form.Item>
        <Form.Item label="Name" {...errorsField.name}>
          {getFieldDecorator('name', {
            initialValue: currentData.display_name || '',
            rules: [
              { required: true, message: 'Name Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                name: undefined,
              })
              return e.target.value
            },
          })(
            <Input className="uppercase" placeholder="Input Name" disabled={isEdit} />,
          )}
        </Form.Item>
        <Form.Item label="Permission" {...errorsField.permission_ids}>
          {getFieldDecorator('permission_ids', {
            initialValue: currentData.permissions ? currentData.permissions.map(o => o.id) : statePermission.list.map(o => o.id),
          })(
            <Checkbox.Group onChange={onChange} value={checkedList}>
              <Row gutter={[12, 12]}>
                {statePermission.list.map(item => (
                  <Col xs={24} md={12} sm={24} key={`chkb_${item.id}`}>
                    <Checkbox value={item.id} disabled={!!['auth-login', 'forgot-password', 'change-password', 'signout', 'password-update', 'password-menu'].includes(item.name)}>{capitalize((item.name).split('-').join(' '))}</Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">{!isEdit ? 'Add Role Group' : 'Submit Change'}</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormRoleGroup.propTypes = {
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  isFetching: PropTypes.bool,
  errorMessage: PropTypes.string,
  errorsField: PropTypes.object,
  checkAll: PropTypes.bool,
  checkedList: PropTypes.array,
  setErrorsField: PropTypes.func,
  currentData: PropTypes.object,
  statePermission: PropTypes.object,
  onChange: PropTypes.func,
  onCheckAllChange: PropTypes.func,
}

export default FormRoleGroup
