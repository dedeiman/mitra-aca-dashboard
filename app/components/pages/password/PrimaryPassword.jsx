import React from 'react'
import PropTypes from 'prop-types'
import {
  Form, Divider,
  Input, Button,
  Card, Alert,
} from 'antd'

const PrimaryPasswordChange = ({
  onSubmit, form, isLoading,
  isSecondary, toggleSecondary,
  onVerify, stateError,
}) => {
  const { getFieldDecorator } = form
  return (
    <Card className="h-100">
      <Form onSubmit={onSubmit}>
        <p className="title-card mb-4">Primary Password</p>
        <p className="font-weight-bold">Password Anda Saat Ini</p>
        <Form.Item {...stateError.old_password}>
          {getFieldDecorator('old_password', {
            initialValue: undefined,
            rules: [
              { required: true, message: '*Wajib diisi' },
              { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' },
            ],
          })(
            <Input.Password
              placeholder="Masukan Password..."
              className="field-lg"
              maxLength={50}
              iconRender={visible => (
                visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
              )}
              onChange={async (e) => {
                const val = e.target.value
                await onVerify(val)
              }}
            />,
          )}
        </Form.Item>
        <p className="font-weight-bold">Password Baru Anda</p>
        <Form.Item>
          {getFieldDecorator('password', {
            initialValue: undefined,
            rules: [
              { required: true, message: '*Wajib diisi' },
              { pattern: /(?=.*[A-Z])/, message: '*Wajib memiliki minimal 1 huruf besar' },
              { pattern: /(?=.*[a-z])/, message: '*Wajib memiliki minimal 1 huruf kecil' },
              { pattern: /(?=.*\d)/, message: '*Wajib memiliki minimal 1 angka' },
              { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' }, {
                validator: (rule, value) => {
                  // eslint-disable-next-line prefer-promise-reject-errors
                  if (value && value.includes(' ')) return Promise.reject('*Tidak boleh menggunakan space')
                  // eslint-disable-next-line prefer-promise-reject-errors
                  return Promise.resolve()
                },
              },
            ],
          })(
            <Input.Password
              placeholder="Password Baru"
              className="field-lg"
              maxLength={50}
              iconRender={visible => (
                visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
              )}
            />,
          )}
        </Form.Item>
        <p className="font-weight-bold">Konfirmasi Password</p>
        <Form.Item>
          {getFieldDecorator('password_confirmation', {
            initialValue: undefined,
            rules: [
              { required: true, message: '*Wajib diisi' },
              { pattern: /(?=.*[A-Z])/, message: '*Wajib memiliki minimal 1 huruf besar' },
              { pattern: /(?=.*[a-z])/, message: '*Wajib memiliki minimal 1 huruf kecil' },
              { pattern: /(?=.*\d)/, message: '*Wajib memiliki minimal 1 angka' },
              { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' },
              {
                validator: (rule, value) => {
                  // eslint-disable-next-line prefer-promise-reject-errors
                  if (value && value.includes(' ')) return Promise.reject('*Tidak boleh menggunakan space')
                  // eslint-disable-next-line prefer-promise-reject-errors
                  return Promise.resolve()
                },
              },
            ],
          })(
            <Input.Password
              placeholder="Konfirmasi Password"
              className="field-lg"
              maxLength={50}
              iconRender={visible => (
                visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
              )}
            />,
          )}
        </Form.Item>
        {stateError.message && (
          <Alert
            message="Error"
            description={stateError.message}
            type="error"
            closable
            // onClose={onCloseError}
            className="mb-3 text-left"
          />
        )}
        <Form.Item>
          <Button type="primary button-lg float-right mt-4" htmlType="submit" disabled={isLoading}>
            Ubah Password
          </Button>
        </Form.Item>
        <Divider />
        {!isSecondary && (
          <p>
            Change Second Password ?
            <Button
              type="link"
              className="p-0 pl-1"
              onClick={() => toggleSecondary(true)}
            >
              Click Here!
            </Button>
          </p>
        )}
      </Form>
    </Card>
  )
}

PrimaryPasswordChange.propTypes = {
  form: PropTypes.any,
  isSecondary: PropTypes.bool,
  toggleSecondary: PropTypes.func,
  isLoading: PropTypes.bool,
  onVerify: PropTypes.func,
  stateError: PropTypes.object,
  setStateError: PropTypes.func,
  onSubmit: PropTypes.func,
}

export default PrimaryPasswordChange
