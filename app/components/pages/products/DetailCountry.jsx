/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form,
  Button, Input,
  Row, Col, Checkbox,
} from 'antd'
import history from 'utils/history'

const DetailCountry = ({
  handleSubmit, form,
  isEdit, detailCountry,
  errorsField,
}) => {
  const {
    getFieldDecorator, validateFields,
    getFieldValue, setFieldsValue,
  } = form

  return (
    <Card title={`Form ${isEdit ? 'Edit' : 'Create'} Country`}>
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item label="Country Code" {...errorsField.code}>
              {getFieldDecorator('code', {
                initialValue: detailCountry.code || '',
                rules: [{ required: true }],
              })(
                <Input placeholder="Input Code" maxLength={8} />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Country Name" {...errorsField.name}>
              {getFieldDecorator('name', {
                initialValue: detailCountry.name || '',
                rules: [{ required: true }],
              })(
                <Input placeholder="Input Name" />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Zone" {...errorsField.zone}>
              {getFieldDecorator('zone', {
                initialValue: detailCountry.zone || '',
                rules: [{ required: true }],
              })(
                <Input placeholder="Input Zone" />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Row className="mt-5">
              <Col span={12}>
                <Form.Item>
                  {getFieldDecorator('is_schengen_area', {
                    rules: [{ required: false }],
                    getValueFromEvent: (e) => {
                      setFieldsValue({
                        is_schengen_area: e.target.checked,
                      })

                      return e.target.checked
                    },
                  })(
                    <Checkbox checked={getFieldValue('is_schengen_area')}>
                      Schengen Area
                    </Checkbox>,
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item>
                  {getFieldDecorator('is_closed_country', {
                    rules: [{ required: false }],
                    getValueFromEvent: (e) => {
                      setFieldsValue({
                        is_closed_country: e.target.checked,
                      })

                      return e.target.checked
                    },
                  })(
                    <Checkbox checked={getFieldValue('is_closed_country')}>
                      Closed Country
                    </Checkbox>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

DetailCountry.propTypes = {
  isEdit: PropTypes.bool,
  form: PropTypes.any,
  errorsField: PropTypes.object,
  detailCountry: PropTypes.object,
  handleSubmit: PropTypes.func,
}

export default DetailCountry
