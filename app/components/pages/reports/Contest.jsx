import PropTypes from 'prop-types'
import {
  Col, Row,
  Card, Table,
  Select, Form,
  Button, Alert,
} from 'antd'

const ContestPage = ({
  stateContest,
  isFetching, handlePage,
  metaContest, dataContest,
  closeError, handleFilter,
  errorMessage, handleExport,
  stateContestYears,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'No',
      // width: 60,
      render: (text, record, idx) => (metaContest.current_page > 1 ? (metaContest.current_page - 1) * 10 + (idx + 1) : idx + 1),
    },
    {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-'),
    },
    {
      title: 'Agent Level',
      dataIndex: 'agent_level',
      key: 'agent_level',
      render: text => (text || '-'),
    },
    {
      title: 'Agent Branch',
      dataIndex: 'agent_branch',
      key: 'agent_branch',
      render: text => (text || '-'),
    },
    {
      title: 'Agent Representative',
      dataIndex: 'agent_representative',
      key: 'agent_representative',
      render: text => (text || '-'),
    },
    {
      title: 'Contest',
      key: 'contest',
      children: [
        {
          title: 'Name',
          dataIndex: 'contest_name',
          key: 'contest_name',
          render: text => (text || '-'),
        },
        {
          title: 'Year',
          dataIndex: 'contest_year',
          key: 'contest_year',
          render: text => (text || '-'),
        },
      ],
    },
    {
      title: 'Reward',
      dataIndex: 'reward',
      key: 'reward',
      render: text => (text || '-'),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h3 className="m-0">Report Contest Winners</h3>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="rounded-pill float-md-right mt-3 mt-md-0"
              onClick={handleExport}
            >
              Export Data
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}
      <Table
        bordered
        rowKey={Math.random()}
        columns={columns}
        dataSource={dataContest}
        loading={isFetching}
        title={() => (
          <Row gutter={12} className="w-100">
            <Col xs={24} md={8} lg={4}>
              <Form.Item label="Filter By Year" className="m-0 mb-3 mb-md-0">
                <Select
                  allowClear
                  showSearch
                  className="w-100"
                  placeholder="Select Year"
                  optionFilterProp="children"
                  loading={stateContestYears.isFetching}
                  onChange={e => handleFilter(e, 'selectedYear')}
                  value={stateContestYears.selectedYear}
                >
                  {stateContestYears.optionYears && stateContestYears.optionYears.map(item => <Select.Option key={item.contest_year} value={item.contest_year}>{item.contest_year}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
            <Col xs={24} md={8} lg={4}>
              <Form.Item label="Filter By Contest" className="m-0 mb-3 mb-md-0" style={{ lineHeight: 'normal' }}>
                <Select
                  allowClear
                  showSearch
                  className="w-100"
                  placeholder="Select Contest"
                  optionFilterProp="children"
                  loading={stateContest.isFetching}
                  onChange={e => handleFilter(e, 'selected')}
                  value={stateContest.selected}
                >
                  {stateContest.options && stateContest.options.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
        )}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaContest ? metaContest.total_count : dataContest.length,
          current: metaContest ? metaContest.current_page : 1,
          onChange: (current) => {
            handlePage(current)
          },
          simple: isMobile,
        }}
      />
    </Card>
  )
}

ContestPage.propTypes = {
  isFetching: PropTypes.bool,
  dataContest: PropTypes.array,
  metaContest: PropTypes.object,
  handlePage: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleFilter: PropTypes.func,
  stateContest: PropTypes.object,
  handleExport: PropTypes.func,
  stateContestYears: PropTypes.object,
}

export default ContestPage
