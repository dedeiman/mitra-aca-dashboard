import PropTypes from 'prop-types'
import {
  Col, Row,
  Card, Table,
  Select, Form,
  Button, Alert,
} from 'antd'
import moment from 'moment'

const ProductsPage = ({
  isFetching, handlePage,
  metaProduct, dataProduct,
  closeError, handleFilter,
  stateBranches, stateProducts,
  errorMessage, handleExport,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'No',
      // width: 60,
      render: (text, record, idx) => (metaProduct.current_page > 1 ? (metaProduct.current_page - 1) * 10 + (idx + 1) : idx + 1),
    },
    {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-'),
    },
    {
      title: 'Agent Level',
      dataIndex: 'agent_level',
      key: 'agent_level',
      render: text => (text || '-'),
    },
    {
      title: 'Agent Branch',
      dataIndex: 'agent_branch',
      key: 'agent_branch',
      render: text => (text || '-'),
    },
    {
      title: 'Agent Representative',
      dataIndex: 'agent_representative',
      key: 'agent_representative',
      render: text => (text || '-'),
    },
    {
      title: 'Class of Business',
      dataIndex: 'class_of_business',
      key: 'class_of_business',
      render: text => (text || '-'),
    },
    {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      render: text => (text || '-'),
    },
    {
      title: 'Expiry Date',
      dataIndex: 'expiry_date',
      key: 'expiry_date',
      render: text => (text ? moment(text).format('YYYY-MM-DD') : '-'),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h3 className="m-0">Report Restricted Product</h3>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="rounded-pill float-md-right mt-3 mt-md-0"
              onClick={handleExport}
            >
              Export Data
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}
      <Table
        bordered
        rowKey={Math.random()}
        columns={columns}
        dataSource={dataProduct}
        loading={isFetching}
        title={() => (
          <Row gutter={12} className="w-100">
            <Col xs={24} md={8} lg={4}>
              <Form.Item label="Filter By Product" className="m-0 mb-3 mb-md-0">
                <Select
                  allowClear
                  showSearch
                  className="w-100"
                  placeholder="Select Product"
                  optionFilterProp="children"
                  loading={stateProducts.isFetching}
                  onChange={e => handleFilter(e, 'Products')}
                  value={stateProducts.selected}
                >
                  {stateProducts.options && stateProducts.options.map(item => <Select.Option key={item.id} value={item.id}>{item.display_name}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
            <Col xs={24} md={8} lg={4}>
              <Form.Item label="Filter By Branch" className="m-0 mb-3 mb-md-0" style={{ lineHeight: 'normal' }}>
                <Select
                  allowClear
                  showSearch
                  className="w-100"
                  placeholder="Select Branch"
                  optionFilterProp="children"
                  loading={stateBranches.isFetching}
                  onChange={e => handleFilter(e, 'Branches')}
                  value={stateBranches.selected}
                >
                  {stateBranches.options && stateBranches.options.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
        )}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaProduct ? metaProduct.total_count : dataProduct.length,
          current: metaProduct ? metaProduct.current_page : 1,
          onChange: handlePage,
          simple: isMobile,
        }}
      />
    </Card>
  )
}

ProductsPage.propTypes = {
  isFetching: PropTypes.bool,
  dataProduct: PropTypes.array,
  metaProduct: PropTypes.object,
  handlePage: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleFilter: PropTypes.func,
  stateBranches: PropTypes.object,
  stateProducts: PropTypes.object,
  handleExport: PropTypes.func,
}

export default ProductsPage
