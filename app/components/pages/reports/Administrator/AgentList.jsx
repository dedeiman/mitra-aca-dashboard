import React from 'react'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button, Alert,
} from 'antd'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import moment from 'moment'

const { Option } = Select

const AgentList = ({
  stateDate, isFetching,
  errorMessage, closeError,
  metaAgentList, handleFilter,
  agentList, state, handlePage,
  handleTableChange, handleExport,
  handleSearch,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'No',
      render: (text, record, idx) => (metaAgentList.current_page > 1 ? (metaAgentList.current_page - 1) * 10 + (idx + 1) : idx + 1),
    },
    {
      title: 'Agent ID',
      dataIndex: 'agent_id',
    },
    {
      title: 'Profile ID',
      dataIndex: 'profile_id',
    },
    {
      title: 'Agent Name',
      dataIndex: 'name',
    },
    {
      title: 'Agent Branch',
      dataIndex: ['branch', 'name'],
    },
    {
      title: 'Agent Representative',
      dataIndex: ['branch_perwakilan', 'name'],
    },
    {
      title: 'Agent ID Upline',
      dataIndex: 'upline_id',
    },
    {
      title: 'Upline Name',
      dataIndex: 'upline',
    },
    {
      title: 'Profile ID Upline',
      dataIndex: ['upline', 'profile_id'],
    },
    {
      title: 'Agent Level',
      dataIndex: ['level', 'name'],
    },
    {
      title: 'Agent Type',
      dataIndex: 'type',
    },
    {
      title: 'Agent Status',
      dataIndex: 'status',
    },
    {
      title: 'Gender',
      dataIndex: 'gender',
    },
    {
      title: 'Birth Place',
      dataIndex: 'birth_place',
    },
    {
      title: 'Birth Date',
      dataIndex: 'birthdate',
    },
    {
      title: 'NPWP No',
      dataIndex: 'npwp',
    },
    {
      title: 'Status PTKP',
      dataIndex: 'status',
    },
    {
      title: 'e-KTP No',
      dataIndex: 'no_ktp',
    },
    {
      title: 'AAUI Certificate No',
      dataIndex: 'licenses',
      render: licenses => (
        <>
          {licenses.map(license => <p>{license.license_number}</p>)}
        </>
      ),
    },
    {
      title: 'Contract No.',
      dataIndex: 'contracts',
      render: contracts => (
        <>
          {contracts.map(contract => <p>{contract.contract_number}</p>)}
        </>
      ),
    },
    {
      title: 'Contract Expired Date',
      dataIndex: 'contracts',
      render: contracts => (
        <>
          {contracts.map(contract => <p>{moment(contract.end_date).format('YYYY-MM-DD')}</p>)}
        </>
      ),
    },
    {
      title: 'Join Date',
      dataIndex: 'created_at',
      render: createdAt => (
        <p>{moment(createdAt).format('YYYY-MM-DD')}</p>
      ),
    },
    {
      title: 'KTP Address',
      dataIndex: 'address',
    },
    {
      title: 'Address',
      dataIndex: 'address',
    },
    {
      title: 'Status PTKP',
      dataIndex: 'age',
    },
    {
      title: 'Phone No',
      dataIndex: 'phone_number',
    },
    {
      title: 'Email Address',
      dataIndex: 'email',
    },
    {
      title: 'Religion',
      dataIndex: 'religion',
    },
    {
      title: 'Employment',
      dataIndex: 'employment',
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h3 className="m-0">Report Agent List</h3>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="rounded-pill float-md-right mt-3 mt-md-0"
              onClick={handleExport}
            >
              Export Data
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}
      <Table
        bordered
        rowKey={Math.random()}
        columns={columns}
        dataSource={agentList}
        scroll={{ x: 'max-content' }}
        loading={isFetching}
        onChange={handleTableChange}
        title={() => (
          <>
            <Row gutter={12} className="w-100">
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Join Date" className="m-0 text-truncate" style={{ lineHeight: 'normal' }}>
                  <DatePicker.RangePicker
                    allowClear
                    placeholder={['Start Date', 'End Date']}
                    onChange={e => handleFilter(e, 'selectedJoinDate')}
                    value={stateDate.selectedJoinDate}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="License Date" className="m-0 text-truncate" style={{ lineHeight: 'normal' }}>
                  <DatePicker.RangePicker
                    allowClear
                    placeholder={['Start Date', 'End Date']}
                    onChange={e => handleFilter(e, 'selectedLicenseDate')}
                    value={stateDate.selectedLicenseDate}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Expired Type" className="mb-2">
                  <Select
                    placeholder="- Expired Type -"
                    className="w-100"
                    value={state.license_type || undefined}
                    onChange={val => handleFilter(val, 'license_type')}
                    allowClear
                  >
                    <Option value="aaui" key={Math.random()}>AAUI</Option>
                    <Option value="mou" key={Math.random()}>MOU</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Agent Type" className="mb-2">
                  <Select
                    allowClear
                    placeholder="- Agent Type -"
                    className="w-100"
                    value={state.agent_type || undefined}
                    onChange={val => handleFilter(val, 'agent_type')}
                  >
                    {
                      (state.agent_typeList || []).map(key => (
                        <Option value={key.value} key={Math.random()}>{key.label}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={12} className="w-100">
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Level" className="mb-2">
                  <Select
                    allowClear
                    placeholder="- Level -"
                    className="w-100"
                    value={state.level_id || undefined}
                    onChange={val => handleFilter(val, 'level_id')}
                  >
                    {
                      (state.levelList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Branch" className="mb-2">
                  <Select
                    allowClear
                    placeholder="- Branch -"
                    className="w-100"
                    value={state.branch_id || undefined}
                    onChange={val => handleFilter(val, 'branch_id')}
                  >
                    {
                      (state.branchList || []).map(key => (
                        <Option value={key.id} key={key.id}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Agent Status" className="mb-2">
                  <Select
                    allowClear
                    placeholder="- Agent Status -"
                    className="w-100"
                    value={state.status || undefined}
                    onChange={val => handleFilter(val, 'status')}
                  >
                    <Option value="active" key={Math.random()}>Active</Option>
                    <Option value="inactive" key={Math.random()}>Inactive</Option>
                    <Option value="passed-away" key={Math.random()}>Passed Away</Option>
                    <Option value="restricted" key={Math.random()}>Restricted</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Birth Month" className="mb-2">
                  <Select
                    allowClear
                    placeholder="- Birth Month -"
                    className="w-100"
                    value={state.mob || undefined}
                    onChange={val => handleFilter(val, 'mob')}
                  >
                    {(moment.months() || []).map((item, idx) => (
                      <Select.Option key={Math.random()} value={idx + 1}>{item}</Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={12} className="w-100">
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Religion" className="mb-2">
                  <Select
                    allowClear
                    placeholder="- Religion -"
                    className="w-100"
                    value={state.religion || undefined}
                    onChange={val => handleFilter(val, 'religion')}
                  >
                    <Option value="islam" key={Math.random()}>Islam</Option>
                    <Option value="protestan" key={Math.random()}>Protestan</Option>
                    <Option value="katolik" key={Math.random()}>Katolik</Option>
                    <Option value="hindu" key={Math.random()}>Hindu</Option>
                    <Option value="budha" key={Math.random()}>Budha</Option>
                    <Option value="khonghucu" key={Math.random()}>Khonghucu</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} md={12} lg={6}>
                <Form.Item label="Search Agent" className="mb-2">
                  <Input
                    allowClear
                    className="w-100"
                    value={state.search}
                    placeholder="Search Agent ID / Profile ID / Name Agent..."
                    onChange={val => handleFilter(val.target.value, 'search')}
                  />
                </Form.Item>
              </Col>
            </Row>
          </>
        )}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaAgentList ? metaAgentList.total_count : agentList.list.data.length,
          current: metaAgentList ? metaAgentList.current_page : 1,
          simple: isMobile,
          onChange: (current) => {
            handlePage(current)
          },
        }}
      />
    </Card>
  )
}

AgentList.propTypes = {
  agentList: PropTypes.any,
  isFetching: PropTypes.bool,
  state: PropTypes.object,
  handleTableChange: PropTypes.func,
  errorMessage: PropTypes.string,
  closeError: PropTypes.func,
  metaAgentList: PropTypes.object,
  handleExport: PropTypes.func,
  handleFilter: PropTypes.func,
  stateDate: PropTypes.object,
  handlePage: PropTypes.func,
  handleSearch: PropTypes.func,
}

export default AgentList
