import PropTypes from 'prop-types'

import {
  Layout, Breadcrumb,
} from 'antd'
import { Sidebar } from '../elements'

const { Content } = Layout

const PrivateLayout = ({
  children, doLogout,
  handleKeys, site, currentUser,
  isCollapsed, toggleCollapsed,
  imgAvatar, setAvatar,
}) => (
  <div className="app main">
    <div id="yield" className="app-main-body">
      <Layout>
        <Layout>
          <Sidebar
            site={site}
            user={currentUser || {}}
            logout={doLogout}
            isCollapsed={isCollapsed}
            toggleCollapsed={toggleCollapsed}
            handleKeys={(param, isOpenSub) => handleKeys(param, isOpenSub)}
            imgAvatar={imgAvatar}
            setAvatar={setAvatar}
          />
          <Layout className="main-content" style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              {(site.breadList || []).map(item => (
                <Breadcrumb.Item key={Math.random()}>{item}</Breadcrumb.Item>
              ))}
            </Breadcrumb>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
                background: '/assets/city-bg.svg',
              }}
            >
              {children}
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </div>
  </div>
)

PrivateLayout.propTypes = {
  children: PropTypes.node,
  doLogout: PropTypes.func,
  handleKeys: PropTypes.func,
  site: PropTypes.object,
  currentUser: PropTypes.object,
  isCollapsed: PropTypes.bool,
  toggleCollapsed: PropTypes.func,
  imgAvatar: PropTypes.string,
  setAvatar: PropTypes.func,
}

export default PrivateLayout
