import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withHandlers, lifecycle, withState,
} from 'recompose'
import {
  forgotPassword,
  authenticateUserFailure,
  resetPassword,
  otpPassword,
} from 'actions/Auth'
import {
  startCase,
} from 'lodash'
import Swal from 'sweetalert2'
import history from 'utils/history'
import { getDatas } from 'actions/Option'
import { Form, message } from 'antd'
import qs from 'query-string'
import ForgotView from 'components/login/Forgot'

const mapDispatchToProps = dispatch => ({
  forgotPassword: bindActionCreators(forgotPassword, dispatch),
  resetPassword: bindActionCreators(resetPassword, dispatch),
  otpPassword: bindActionCreators(otpPassword, dispatch),
  authenticateUserFailure: bindActionCreators(authenticateUserFailure, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'loginForm' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('isAuthenticating', 'setAuthenticating', false),
    withState('isPassword', 'setPassword', 'reset'),
    withState('errorMessage', 'setErrorMessage', ''),
    withState('errorsField', 'setErrorsField', {}),
    withState('tokenExpired', 'setTokenExpired', ''),
    withHandlers({
      onCloseError: props => () => {
        props.setErrorMessage('')
      },
      onChange: props => (e) => {
        const event = e.target || e
        const { name, value } = event

        props.form.setFieldsValue({
          [name]: { value },
        })
      },
      handleResendOTP: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/passwords/reset/resend-otp/${window.localStorage.getItem('isToken')}`, method: 'post' },
        ).then((res) => {
          message.success(res.meta.message)
        }).catch((err) => {
          message.error(err.message)
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const {
          setAuthenticating, form, isPassword,
          setErrorMessage, location,
        } = props

        form.validateFields((err, values) => {
          if (!err) {
            setAuthenticating(true)
            let payload = values
            if (isPassword === 'reset') {
              payload = {
                ...payload,
                reset_token: qs.parse(location.search).tkn,
                change_password: !qs.parse(location.search).is_secondary || false,
                change_alternative_password: qs.parse(location.search).is_secondary === 'true' || false,
              }
            }
            if (isPassword === 'otp') {
              payload = {
                ...values,
                token: window.localStorage.getItem('isToken'),
              }
            }
            props[`${isPassword}Password`](payload)
              .then((res) => {
                setAuthenticating(false)

                if (isPassword === 'forgot') {
                  if (res.is_otp_verification) {
                    window.localStorage.setItem('isToken', res.token)
                    props.getDatas(
                      { base: 'apiUser', url: '/passwords/reset/send-otp', method: 'post' }, {
                        token: res.token,
                        send_via: 'sms',
                      },
                    ).then(() => {
                      Swal.fire(
                        'Link to Reset Password has been sent',
                        `Check your ${res.is_otp_verification ? 'handphone' : 'email'}`,
                        'success',
                      ).then(() => {
                        if (res.is_otp_verification) {
                          history.push('/password/otp')
                        } else {
                          history.push('/')
                        }
                      })
                    }).catch((errors) => {
                      setAuthenticating(false)
                      if (errors.errors && Object.keys(errors.errors).length) {
                        let objError = {}
                        Object.keys(errors.errors).forEach((item) => {
                          objError = {
                            ...objError,
                            [item]: {
                              validateStatus: 'error',
                              help: startCase((errors.errors[item]).replace('_id', '').replace('_', ' ')),
                            },
                          }
                          props.setErrorsField(objError)
                        })
                      } else {
                        Swal.fire(
                          'Error!',
                          errors.message,
                          'error',
                        )
                      }
                    })
                  }
                }
              })
              .catch((error) => {
                setAuthenticating(false)
                setErrorMessage(error)
              })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        const {
          setPassword, match,
          location, setTokenExpired,
        } = this.props
        this.props.authenticateUserFailure('')
        setPassword(match.params.type)
        if (match.params.type === 'reset') {
          this.props.getDatas(
            { base: 'apiUser', url: '/passwords/token-validation', method: 'post' },
            { token: qs.parse(location.search).tkn },
          ).catch((err) => {
            setTokenExpired(err.message)
          })
        }
      },
    }),
  )(ForgotView),
)
