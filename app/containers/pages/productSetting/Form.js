import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withHandlers,
  lifecycle, withState,
} from 'recompose'
import {
  fetchDetailCustomer, customerDetail,
  createCustomer, updateCustomer,
} from 'actions/ProductSetting'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
// import { isEmpty, pickBy, identity } from 'lodash'
import FormCustomer from 'components/pages/customers/Form'
import history from 'utils/history'
import Swal from 'sweetalert2'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailCustomer,
  } = state.root.customer

  return {
    isFetching,
    detailCustomer,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailCustomer: bindActionCreators(fetchDetailCustomer, dispatch),
  customerDetail: bindActionCreators(customerDetail, dispatch),
  createCustomer: bindActionCreators(createCustomer, dispatch),
  updateCustomer: bindActionCreators(updateCustomer, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCustomer' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateSelects', 'setStateSelects', {}),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withHandlers({
      loadLocation: props => (url, name) => {
        props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
    withHandlers({
      handleLocation: props => (url, key) => {
        props.loadLocation(url, key)
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { params } = props.match
        props.form.validateFields((err, values) => {
          if (!err) {
            let { npwp } = values
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)
            npwp = npwp.split('-').join('')
            npwp = npwp.split('.').join('')
            npwp = npwp.split(' ').join('')

            const payload = {
              ...values,
              number_id: values.number_id ? (values.number_id).toUpperCase() : '',
              pic_name: values.pic_name ? (values.pic_name).toUpperCase() : '',
              other_employment: values.other_employment ? (values.other_employment).toUpperCase() : '',
              birthplace: values.birthplace ? (values.birthplace).toUpperCase() : '',
              name: values.name ? (values.name).toUpperCase() : '',
              dob: values.dob ? moment(values.dob).format('YYYY-MM-DD') : '',
              lat: values.lat ? Number(values.lat) : 0,
              lng: values.lng ? Number(values.lng) : 0,
              phone_code_id: codeId[0].id,
              expiry_date_identity_id: values.expiry_date_identity_id !== null ? moment(values.expiry_date_identity_id).format('YYYY-MM-DD') : '',
              npwp,
            }
            props[`${params.id ? 'upd' : 'cre'}ateCustomer`](payload, (params.id || null))
              .then(() => (
                Swal.fire(`Customer telah di${params.id ? 'perbarui' : 'tambahkan'}`, 'Kembali ke List Customer', 'success')
                  .then(() => history.push('/customer'))
              ))
              .catch(error => (
                Swal.fire(error, '', 'error')
              ))
          } else {
            Swal.fire(
              'Terjadi kesalahan',
              'Mohon Periksa kembali data Anda !',
              'error',
            )
          }
        })
      },
    }),
    lifecycle({
      componentWillMount() {
        const { params } = this.props.match
        const config = [
          { url: '/marital-statuses', name: 'marital' },
          { url: '/employments', name: 'employment' },
          { url: '/range-of-incomes', name: 'income' },
          { url: '/provinces', name: 'provincy' },
        ]

        if (params.id) {
          this.props.fetchDetailCustomer(params.id)
        } else {
          this.props.customerDetail({})
        }


        config.map(item => (
          this.loadMasterData(item.url, item.name)
        ))
      },
      componentDidMount() {
        window.google.maps.event.addDomListener(window, 'load', this.initAutocomplete()) // eslint-disable-line

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: [],
          })
        })
      },
      componentDidUpdate(nextProps) {
        const { detailCustomer } = this.props
        if (detailCustomer !== nextProps.detailCustomer) {
          const config = []
          if (detailCustomer.province_id) config.push({ url: `/cities/${detailCustomer.province_id}`, name: 'city' })
          if (detailCustomer.city_id) config.push({ url: `/sub-district/${detailCustomer.city_id}`, name: 'subDistrict' })
          if (detailCustomer.sub_district_id) config.push({ url: `/villages/${detailCustomer.sub_district_id}`, name: 'villages' })

          config.map(item => this.loadMasterData(item.url, item.name))
        }
      },
      loadMasterData(url, name) {
        // const { setStateSelects, stateSelects } = this.props
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
      initAutocomplete() {
        const inputNode = document.getElementById('formCustomer_address')
        const autoComplete = new window.google.maps.places.Autocomplete(inputNode)
        /* eslint-disable */
        window.google.maps.event.addDomListener(inputNode, 'keydown', () => {
          if (event.key === 'Enter') {
            event.preventDefault()
          }
        })
        /* eslint-enable */
        autoComplete.addListener('place_changed', () => {
          const { setFieldsValue } = this.props.form
          const place = autoComplete.getPlace()
          const location = JSON.stringify(place.geometry.location)
          const data = JSON.parse(location)
          data.address = place.formatted_address

          setFieldsValue({
            lat: data.lat,
            lng: data.lng,
            address: data.address,
          })
        })
      },
    }),
  )(FormCustomer),
)
