import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
  withPropsOnChange,
} from 'recompose'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import { debounce, isEmpty } from 'lodash'
import moment from 'moment'
import Helper from 'utils/Helper'
import CardForm from 'components/pages/customers/CardForm'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    detailAgent,
  } = state.root.agent

  return {
    detailAgent,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formCard' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateForm', 'setStateForm', {}),
    withState('stateFile', 'setStateFile', {}),
    withState('imgSrc', 'setImgSrc', null),
    withState('stateModalWebcam', 'setStateModalWebcam', {
      visible: false,
    }),
    withHandlers({
      handleUpload: props => (info, type) => {
        const { stateFile, setStateFile } = props

        Helper.getBase64(info.file, file => setStateFile({ ...stateFile, [type]: file }))
      },
      handleDeleteWebcamPict: props => () => {
        props.setImgSrc(null)
      },
      handleSearchProduct: props => (val) => {
        props.getDatas(
          { base: 'apiUser', url: `/all-products?keyword=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateForm({
            ...props.stateForm,
            productLoad: false,
            productList: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateForm({
            ...props.stateForm,
            productLoad: false,
            productList: [],
          })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            const { title, isEdit, data: currentData } = props.data
            let payload = {}

            if (title.toLowerCase() === 'product') {
              payload = {
                id: isEdit ? currentData.id : '',
                product: (props.stateForm.productList || []).find(item => item.id === values.product_id),
                product_id: values.product_id,
                restricted_date: values.restricted_date ? moment(values.restricted_date).format('YYYY-MM-DD') : '',
                unrestricted_date: values.unrestricted_date ? moment(values.unrestricted_date).format('YYYY-MM-DD') : '',
                reason: values.reason,
                status: values.status,
              }
            }
            if (title.toLowerCase() === 'agent') {
              payload = {
                id: isEdit ? currentData.id : null,
                agent_id: isEdit ? currentData.id : '',
                status: values.status,
              }
            }
            if (title.toLowerCase() === 'document') {
              payload = {
                id: isEdit ? currentData.id : '',
                document_category_id: values.document_category_id,
                description: values.description,
                file_url: values.file || !isEmpty(props.stateFile.file) ? props.stateFile.file : currentData.file_url,
              }
            }

            props.submitForm(payload, title, isEdit)
          }
        })
      },
    }),
    withPropsOnChange(
      ['handleSearchProduct'],
      ({ handleSearchProduct }) => ({
        handleSearchProduct: debounce(handleSearchProduct, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        const { data, setStateFile, stateFile } = this.props
        let url = ''
        if ((data.title).toLowerCase() === 'product') url = '/all-products?keyword='
        if ((data.title).toLowerCase() === 'contract') url = '/contract_types'
        if ((data.title).toLowerCase() === 'license') url = '/license_types'
        if ((data.title).toLowerCase() === 'bank') url = '/banks'
        if ((data.title).toLowerCase() === 'document') url = '/documents/categories'
        if ((data.title).toLowerCase() === 'taxation') {
          url = '/tax-types'
          this.loadMasterData({
            base: 'apiUser',
            method: 'get',
            url: '/tax-ptkp-statuses',
            name: 'ptkp',
          })
        }
        if (url) {
          this.loadMasterData({
            url,
            base: 'apiUser',
            method: 'get',
            name: (data.title).toLowerCase(),
          })
        }
        if (data.isEdit && data.data.file_url) {
          setStateFile({ ...stateFile, file: data.data.file_url })
        } else {
          setStateFile({ ...stateFile, file: '' })
        }
      },
      componentDidUpdate(prevProps) {
        const { data, setStateFile, stateFile } = this.props
        let url = ''
        if (data !== prevProps.data) {
          if ((data.title).toLowerCase() === 'product') url = '/all-products?keyword='
          if ((data.title).toLowerCase() === 'contract') url = '/contract_types'
          if ((data.title).toLowerCase() === 'license') url = '/license_types'
          if ((data.title).toLowerCase() === 'bank') url = '/banks'
          if ((data.title).toLowerCase() === 'document') url = '/documents/categories'
          if ((data.title).toLowerCase() === 'taxation') {
            url = '/tax-types'
            this.loadMasterData({
              base: 'apiUser',
              method: 'get',
              url: '/tax-ptkp-statuses',
              name: 'ptkp',
            })
          }
          if (url) {
            this.loadMasterData({
              url,
              base: 'apiUser',
              method: 'get',
              name: (data.title).toLowerCase(),
            })
          }

          if (data.isEdit && data.data.file_url) {
            setStateFile({ ...stateFile, file: data.data.file_url })
          } else {
            setStateFile({ ...stateFile, file: '' })
          }
        }
      },
      loadMasterData(data) {
        this.props.getDatas(
          { base: data.base, url: data.url, method: data.method },
        ).then((res) => {
          this.props.setStateForm({
            ...this.props.stateForm,
            [`${data.name}Load`]: false,
            [`${data.name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateForm({
            ...this.props.stateForm,
            [`${data.name}Load`]: false,
            [`${data.name}List`]: [],
          })
        })
      },
    }),
  )(CardForm),
)
