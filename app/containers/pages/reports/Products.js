import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchReportProduct, reportProductFailure } from 'actions/Report'
import { getBranches, getProducts } from 'actions/Option'
import ProductsView from 'components/pages/reports/Product'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataProduct,
    metaProduct,
  } = state.root.reports

  return {
    isFetching,
    errorMessage,
    dataProduct,
    metaProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchReportProduct: bindActionCreators(fetchReportProduct, dispatch),
  reportProductFailure: bindActionCreators(reportProductFailure, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
  getProducts: bindActionCreators(getProducts, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateProducts', 'setStateProducts', {
    isFetching: true,
    options: [],
    selected: undefined,
  }),
  withState('stateBranches', 'setStateBranches', {
    isFetching: true,
    options: [],
    selected: undefined,
  }),
  withHandlers({
    reloadPage: props => () => {
      const payload = qs.stringify({
        product_id: props.stateProducts.selected,
        branch_id: props.stateBranches.selected,
        page: '1',
        per_page: '',
      })
      props.fetchReportProduct(`?${payload}`)
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      const payload = qs.stringify({
        product_id: props.stateProducts.selected,
        branch_id: props.stateBranches.selected,
        page,
      })
      props.fetchReportProduct(`?${payload}`)
    },
    closeError: props => () => props.reportProductFailure(''),
    handleFilter: props => (value, type) => {
      props[`setState${type}`]({
        ...props[`state${type}`],
        selected: value,
      })
      setTimeout(() => {
        props.reloadPage()
      }, 300)
    },
    handleExport: props => () => {
      const payload = qs.stringify({
        product_id: props.stateProducts.selected,
        branch_id: props.stateBranches.selected,
        format: 'excel',
      })
      window.open(
        `${process.env.APP_CONFIG.api_url}/report/agent-restricted-product/download?${payload}`,
        '_blank',
      )
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        stateBranches, setStateBranches,
        stateProducts, setStateProducts,
      } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Report Product'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/restricted-product')
      this.props.fetchReportProduct()
      this.props.getBranches().then((res) => {
        setStateBranches({
          ...stateBranches,
          options: res,
          isFetching: false,
        })
      })
      this.props.getProducts().then((res) => {
        setStateProducts({
          ...stateProducts,
          options: res,
          isFetching: false,
        })
      })
    },
  }),
)(ProductsView)
