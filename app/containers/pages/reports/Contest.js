/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchReportContest, reportContestFailure } from 'actions/Report'
import { getContest, getDatas } from 'actions/Option'
import ContestView from 'components/pages/reports/Contest'
import qs from 'query-string'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataContest,
    metaContest,
  } = state.root.reports

  return {
    isFetching,
    errorMessage,
    dataContest,
    metaContest,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchReportContest: bindActionCreators(fetchReportContest, dispatch),
  reportContestFailure: bindActionCreators(reportContestFailure, dispatch),
  getContest: bindActionCreators(getContest, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateContest', 'setStateContest', {
    isFetching: true,
    options: [],
    selected: undefined,
  }),
  withState('stateContestYears', 'setStateContestYears', {
    isFetching: true,
    optionYears: [],
    selectedYear: undefined,
  }),
  withHandlers({
    reloadPage: props => () => {
      const payload = qs.stringify({
        year: props.stateContestYears.selectedYear ? moment(props.stateContestYears.selectedYear).format('YYYY') : '',
        contest_id: props.stateContest.selected,
        page: '1',
      })
      props.fetchReportContest(`?${payload}`)
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      const payload = qs.stringify({
        year: props.stateContestYears.selectedYear ? moment(props.stateContestYears.selectedYear).format('YYYY') : '',
        contest_id: props.stateContest.selected,
        page,
      })
      props.fetchReportContest(`?${payload}`)
    },
    closeError: props => () => props.reportContestFailure(''),
    handleFilter: props => (value, type) => {
      props.setStateContest({
        ...props.stateContest,
        [type]: value,
      })
      props.setStateContestYears({
        ...props.stateContestYears,
        [type]: value,
      })
      setTimeout(() => {
        props.reloadPage()
      }, 300)
    },
    handleExport: props => () => {
      const payload = qs.stringify({
        year: props.stateContestYears.selectedYear,
        contest_id: props.stateContest.selected,
        format: 'excel',
      })
      window.open(
        `${process.env.APP_CONFIG.api_url_content}/report/contest-winners/download?${payload}`,
        '_blank',
      )
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        stateContest, setStateContest,
        setStateContestYears, stateContestYears,
      } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Report Contest Winners'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/contest-winner')
      this.props.fetchReportContest()
      this.props.getContest().then((res) => {
        setStateContest({
          ...stateContest,
          options: res,
          isFetching: false,
        })
      })
      this.props.getDatas(
        { base: 'apiContent', url: '/report/contest-winners-year', method: 'get' },
      ).then((res) => {
        setStateContestYears({
          ...stateContestYears,
          optionYears: res.data,
          isFetching: false,
        })
      })
    },
  }),
)(ContestView)
