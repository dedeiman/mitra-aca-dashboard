import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import ProductionReport from 'components/pages/reports/Productions/ClaimSettle'
import {
  compose, lifecycle,
} from 'recompose'

export function mapStateToProps(state) {
  const {
  } = state.root

  return {
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('breadList', ['Home', 'Produksi', 'Claim Settle'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/claim-settle')
    },
  }),
)(ProductionReport)
