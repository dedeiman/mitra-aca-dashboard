import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import Training from 'components/pages/reports/Training'
import { fetchTraining } from 'actions/Reports/Training'
import QueryString from 'query-string'
import config from 'config'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import moment from 'moment'
import { isEmpty } from 'lodash'

export function mapStateToProps(state) {
  const {
    trainingReport,
  } = state.root
  return {
    trainingReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchTraining, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    subject: '',
    training: '',
    branch: '',
    per_page: 10,
    isChecked: false,
  }),
  withState('stateTime', 'setStateTime', ['', '']),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list, state, stateTime } = props

      props.changeState({
        ...props.state,
        per_page: page.pageSize,
      })

      list({
        per_page: page.pageSize,
        page: page.current,
        subject_id: state.subject,
        training_id: state.training,
        branch_organizer_id: state.branch,
        from_date: stateTime[0],
        to_date: stateTime[1],
        is_detail: state.isChecked,
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state, stateTime } = props

      list({
        page: 1,
        per_page: state.per_page,
        subject_id: state.subject,
        training_id: state.training,
        branch_organizer_id: state.branch,
        from_date: stateTime[0],
        to_date: stateTime[1],
        is_detail: state.isChecked,
      })
    },
    handleReport: props => () => {
      const { state, stateTime } = props

      const payload = QueryString.stringify({
        subject_id: state.subject,
        training_id: state.training,
        branch_organizer_id: state.branch,
        from_date: stateTime[0],
        to_date: stateTime[1],
        is_detail: state.isChecked,
        format: 'excel',
      })
      window.open(
        `${config.api_url}/report/training-class/download?${payload}`,
        '_blank',
      )
    },
    handleChecked: props => () => {
      const { list, state, stateTime } = props

      props.changeState({
        ...props.state,
        isChecked: !props.state.isChecked,
      })

      list({
        page: 1,
        subject_id: state.subject,
        training_id: state.training,
        branch_organizer_id: state.branch,
        from_date: stateTime[0],
        to_date: stateTime[1],
        per_page: state.per_page,
        is_detail: !state.isChecked,
      })
    },
    handleFilter: props => (value, type) => {
      if (type === 'Time') {
        if (!isEmpty(value)) {
          props.setStateTime([moment(value[0]).format('YYYY-MM-DD'), moment(value[1]).format('YYYY-MM-DD')])
        } else {
          props.setStateTime(['', ''])
        }
      } else {
        props[`setState${type}`]({
          ...props[`state${type}`],
          selected: value,
        })
      }
      setTimeout(() => {
        props.reloadPage()
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: `/report/training-class?per_page=&is_detail=${this.props.state.isChecked}`, name: 'training' },
        { url: '/branches', name: 'branch' },
        { url: '/training-subjects', name: 'subject' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Training', 'Report Training'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/training')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(Training)
