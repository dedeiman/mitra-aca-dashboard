import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import CareerReport from 'components/pages/reports/Career'
import { fetchCareerList, fetchDownloadCareer } from 'actions/Reports/Finance/Career'
import { getBranches, getRepresentativeBranch, getLevel, getAgentStatus } from 'actions/Option'
import { compose, lifecycle, withState, withHandlers } from 'recompose'
import qs from 'query-string'
import { pickBy, identity, first, last } from 'lodash'
import moment from 'moment'

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchCareerList: bindActionCreators(fetchCareerList, dispatch),
  fetchDownloadCareer: bindActionCreators(fetchDownloadCareer, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
  getRepresentativeBranch: bindActionCreators(getRepresentativeBranch, dispatch),
  getLevel: bindActionCreators(getLevel, dispatch),
  getAgentStatus: bindActionCreators(getAgentStatus, dispatch)
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withState('dataList', 'setDataList', {}),
  withState('branchList', 'setBranchList', []),
  withState('representativeBranchList', 'setRepresentativeBranchList', []),
  withState('levelList', 'setLevelList', []),
  withState('statusList', 'setStatusList', []),
  withState('isLoading', 'setIsLoading', true),
  withState('state', 'changeState', {
    start_date: '',
    end_date: '',
    claim_start_date: '',
    claim_end_date: '',
    agent_status: '',
    agent_level: '',
    cob_id: '',
    product_id: '',
    branch_id: '',
    branch_perwakilan_id: '',
    agent_keyword: '',
    format: 'pdf',
    page: '1',
    per_page: '10'
  }),
  withHandlers({
    getList: props => async (params) => {
      const { fetchCareerList, setDataList, setIsLoading } = props
      try {
        const res = await fetchCareerList(params)
        setDataList(res)
        setIsLoading(false)
      } catch (err) {
        setDataList()
        setIsLoading(false)
      }
    },
    handleGetBranches: props => async (params) => {
      const { setBranchList, getBranches, setIsLoading } = props
      try {
        const res = await getBranches(params)
        setBranchList(res)
        setIsLoading(false)
      } catch (err) {
        setBranchList([])
        setIsLoading(false)
      }
    },
    handleGetLevel: props => async (params) => {
      const { setLevelList, getLevel, setIsLoading } = props
      try {
        const res = await getLevel(params)
        setLevelList(res)
        setIsLoading(false)
      } catch (err) {
        setLevelList([])
        setIsLoading(false)
      }
    },
    handleGetStatus: props => async (params) => {
      const { setStatusList, getAgentStatus, setIsLoading } = props
      try {
        const res = await getAgentStatus(params)
        setStatusList(res)
        setIsLoading(false)
      } catch (err) {
        setStatusList([])
        setIsLoading(false)
      }
    }
  }),
  withHandlers({
    handleFilter: props => async (value, type) => {
      const { getRepresentativeBranch, changeState, setRepresentativeBranchList } = props

      if (type.includes('Date')) {
        if (type === 'productionDate') {
          changeState({
            ...props.state,
            start_date: moment(value[0]).format('YYYY-MM-DD'),
            end_date: moment(value[1]).format('YYYY-MM-DD')
          })
        } else {
          changeState({
            ...props.state,
            claim_start_date: moment(first(value)).format('YYYY-MM-DD'),
            claim_end_date: moment(last(value)).format('YYYY-MM-DD')
          })
        }
      } else {
        if (type === 'branch_id') {
          try {
            const res = await getRepresentativeBranch(value)
            setRepresentativeBranchList(res)
          } catch {
            setRepresentativeBranchList()
          }
        }

        changeState({
          ...props.state,
          [type]: value
        })
      }
    },
    handlePage: props => (dataPage) => {
      const { getList, state } = props
      const payload = {
        ...state,
        page: dataPage
      }
      getList(`?${qs.stringify(pickBy(payload, identity))}`)
    },
    handleSubmitFilter: props => (params) => {
      params.preventDefault()
      const { getList, state } = props

      getList(`?${qs.stringify(pickBy(state, identity))}`)
    },
    handleDownloadCareer: props => async (params) => {
      const { fetchDownloadCareer, state, setIsLoading } = props
      const payload = {
        ...state,
        format: params
      }

      try {
        const res = await fetchDownloadCareer(`?${qs.stringify(pickBy(payload, identity))}`)
        let anchor = document.createElement('a')
        anchor.href = res.data.file_url
        anchor.target = '_blank'
        anchor.download = `report-benefit-career.${params}`
        anchor.click()
        setIsLoading(false)
      } catch (err) {
        setIsLoading(false)
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { getList, state, handleGetBranches, handleGetLevel, handleGetStatus, updateSiteConfiguration } = this.props

      getList(`?${qs.stringify(pickBy(state, identity))}`)
      handleGetBranches()
      handleGetLevel()
      handleGetStatus()
      updateSiteConfiguration('breadList', ['Home', 'Finance', 'Career'])
      updateSiteConfiguration('activeSubPage', 'reports')
      updateSiteConfiguration('activePage', 'reports/career')
    }
  })
)(CareerReport)
