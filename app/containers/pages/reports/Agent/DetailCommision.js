import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchCommision } from 'actions/Reports/DetailCommision'
import DetailCommision from 'components/pages/reports/Agent/DetailCommision'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import QueryString from 'query-string'
import config from 'config'
import { getDatas } from 'actions/Option'
import { message } from 'antd'

export function mapStateToProps(state) {
  const {
    commisionReport,
    auth,
  } = state.root
  return {
    auth,
    commisionReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchCommision, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    policy_number: '',
    payment_date_from: '',
    payment_date_to: '',
    periode_date_from: '',
    periode_date_to: '',
    cob: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list, state } = props

      list({
        page: page.current,
        cob_id: state.cob,
        payment_date_from: state.payment_date_from,
        payment_date_to: state.payment_date_to,
        policy_number: state.policy_number,
        periode_date_from: state.periode_date_from,
        periode_date_to: state.periode_date_to,
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        cob_id: state.cob,
        payment_date_from: state.payment_date_from,
        payment_date_to: state.payment_date_to,
        periode_date_from: state.periode_date_from,
        periode_date_to: state.periode_date_to,
        policy_number: state.policy_number,
      })
    },
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        cob_id: state.cob,
        payment_date_from: state.payment_date_from,
        payment_date_to: state.payment_date_to,
        periode_date_from: state.periode_date_from,
        periode_date_to: state.periode_date_to,
        policy_number: state.policy_number,
        format: 'excel',
      })

      props.getData(
        { base: 'apiUser', url: `/agent-commision-report/download?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/class-of-business', name: 'cob' },
      ]
     
      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Agent', 'Detail Komisi'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/agent/detail-commision')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(DetailCommision)
