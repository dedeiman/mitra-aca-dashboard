import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchAgentList } from 'actions/Reports/Administrator/AgentList'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import AgentList from 'components/pages/reports/Administrator/AgentList'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import QueryString from 'query-string'
import { message } from 'antd'
import moment from 'moment'
import { isEmpty } from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    agentList,
    errorMessage,
    metaAgentList,
  } = state.root.agentList

  return {
    isFetching,
    agentList,
    errorMessage,
    metaAgentList,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchAgentList: bindActionCreators(fetchAgentList, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateDate', 'setStateDate', {
    isFetching: true,
    selectedJoinDate: undefined,
    selectedLicenseDate: undefined,
  }),
  withState('state', 'changeState', {
    search: '',
    branch_id: '',
    agent_type: '',
    level_id: '',
    status: '',
    mob: '',
    religion: '',
    join_from_date: '',
    join_to_date: '',
    license_type: '',
    license_from_date: '',
    license_to_date: '',
    page: '1',
  }),
  withHandlers({
    reloadPage: props => () => {
      const payload = QueryString.stringify({
        search: props.state.search,
        branch_id: props.state.branch_id,
        agent_type: props.state.agent_type,
        level_id: props.state.level_id,
        status: props.state.status,
        mob: props.state.mob,
        religion: props.state.religion,
        join_from_date: !isEmpty(props.state.selectedJoinDate) ? moment(props.state.selectedJoinDate[0]).format('YYYY-MM-DD') : '',
        join_to_date: !isEmpty(props.state.selectedJoinDate) ? moment(props.state.selectedJoinDate[1]).format('YYYY-MM-DD') : '',
        license_type: props.state.license_type,
        license_from_date: !isEmpty(props.state.selectedLicenseDate) ? moment(props.state.selectedLicenseDate[0]).format('YYYY-MM-DD') : '',
        license_to_date: !isEmpty(props.state.selectedLicenseDate) ? moment(props.state.selectedLicenseDate[1]).format('YYYY-MM-DD') : '',
        page: props.state.page,
      })
      props.fetchAgentList(`${payload}`)
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      const payload = QueryString.stringify({
        search: props.state.search,
        branch_id: props.state.branch_id,
        agent_type: props.state.agent_type,
        level_id: props.state.level_id,
        status: props.state.status,
        mob: props.state.mob,
        religion: props.state.religion,
        join_from_date: !isEmpty(props.state.selectedJoinDate) ? moment(props.state.selectedJoinDate[0]).format('YYYY-MM-DD') : '',
        join_to_date: !isEmpty(props.state.selectedJoinDate) ? moment(props.state.selectedJoinDate[1]).format('YYYY-MM-DD') : '',
        license_type: props.state.license_type,
        license_from_date: !isEmpty(props.state.selectedLicenseDate) ? moment(props.state.selectedLicenseDate[0]).format('YYYY-MM-DD') : '',
        license_to_date: !isEmpty(props.state.selectedLicenseDate) ? moment(props.state.selectedLicenseDate[1]).format('YYYY-MM-DD') : '',
        page,
      })
      props.fetchAgentList(`${payload}`)
    },
    handleFilter: props => (value, type) => {
      props.setStateDate({
        ...props.stateDate,
        [type]: value,
      })
      props.changeState({
        ...props.state,
        [type]: value,
      })
      setTimeout(() => {
        props.reloadPage()
      }, 300)
    },
    closeError: props => () => props.reportContestFailure(''),
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        format: 'excel',
        search: state.search,
        branch_id: state.branch,
        agent_type: state.agent_type,
        level_id: state.level,
        status: state.status,
        mob: state.mob,
        religion: state.religion,
        join_from_date: state.join_from_date,
        join_to_date: state.join_to_date,
        license_type: state.license_type,
        license_from_date: state.license_from_date,
        license_to_date: state.license_to_date,
      })

      props.getData(
        { base: 'apiUser', url: `/reports/agents/download?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        search: state.search,
        branch_id: state.branch,
        agent_type: state.agent_type,
        level_id: state.level,
        status: state.status,
        mob: state.mob,
        religion: state.religion,
        join_from_date: state.join_from_date,
        join_to_date: state.join_to_date,
        license_type: state.license_type,
        license_from_date: state.license_from_date,
        license_to_date: state.license_to_date,
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/branches', name: 'branch' },
        { url: '/search-types', name: 'agent_type' },
        { url: '/levels', name: 'level' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      this.props.updateSiteConfiguration('activePage', 'reports/agent-list')
      this.props.updateSiteConfiguration('breadList', ['Home', 'Report Agent List'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.fetchAgentList()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(AgentList)
