/* eslint-disable prefer-destructuring */
/* eslint-disable no-shadow */
import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  fetchDetailUser, createUser, updateUser,
  getRole, userDetail,
} from 'actions/User'
import { clearCurrentUser } from 'actions/Auth'
import {
  getCOB, getProducts,
  getBranches, getDatas,
} from 'actions/Option'
import { message, Form } from 'antd'
import Swal from 'sweetalert2'
import FormUserView from 'components/pages/users/Form'
import Helper from 'utils/Helper'
import history from 'utils/history'
import {
  pickBy, identity, isEmpty, startCase, first,
} from 'lodash'

export function mapStateToProps(state) {
  const { currentUser } = state.root.auth
  const {
    isFetching,
    detailUser,
    errorMessage,
    errorObject,
  } = state.root.user
  return {
    currentUser,
    isFetching,
    detailUser,
    errorMessage,
    errorObject,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailUser: bindActionCreators(fetchDetailUser, dispatch),
  createUser: bindActionCreators(createUser, dispatch),
  updateUser: bindActionCreators(updateUser, dispatch),
  userDetail: bindActionCreators(userDetail, dispatch),
  getRole: bindActionCreators(getRole, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
  getProducts: bindActionCreators(getProducts, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  clearCurrentUser: bindActionCreators(clearCurrentUser, dispatch),
})

let currentId = 0

export default Form.create({ name: 'userForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('avatarFileList', 'setAvatarFileList', ''),
    withState('roleForCheckbox', 'setRoleForCheckbox', {}),
    withState('loadRole', 'setLoadRole', false),
    withState('selectOption', 'setSelectOption', []),
    withState('loadGroupRole', 'setLoadGroupRole', true),
    withState('selectGroupOption', 'setSelectGroupOption', []),
    withState('loadCOB', 'setLoadCOB', true),
    withState('selectCOBOption', 'setSelectCOBOption', []),
    withState('loadBranches', 'setLoadBranches', true),
    withState('productType', 'setProductType', {
      options: [],
    }),
    withState('selectBranchesOption', 'setSelectBranchesOption', []),
    withState('stateProducts', 'setStateProducts', []),
    withState('stateBranches', 'setStateBranches', []),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('statePermission', 'setStatePermission', {
      load: true,
      list: [],
    }),
    withState('isEdit', 'setIsEdit', false),
    withState('checkedList', 'setCheckedList', []),
    withState('checkAll', 'setCheckAll', false),
    withState('errorsField', 'setErrorsField', {}),
    withState('currentData', 'setCurrentData', {}),
    withState('currentState', 'setCurrentState', {
      keys: [],
      cob: [],
      productIds: [],
      branch: [],
      perwakilan: [],
    }),
    withHandlers({
      handleResendVerify: props => (id) => {
        props.getDatas(
          { base: 'apiUser', url: `/users/resend-email/${id}`, method: 'post' },
        ).then((res) => {
          Swal.fire(res.data, '', 'success')
        }).catch((error) => {
          Swal.fire(error.message, '', 'error')
        })
      },
      handleUpload: props => (info) => {
        Helper.getBase64(info.file, file => props.setAvatarFileList(file))
      },
      addCard: props => () => {
        const { keys, role_group_id: group } = props.form.getFieldsValue()
        currentId += 1

        const nextKeys = keys.concat(currentId)
        let newState
        if (group === 9) {
          newState = props.stateBranches.concat({ load: false, list: [] })
          props.setStateBranches(newState)
        } else {
          newState = props.stateProducts.concat({ load: false, list: [] })
          props.setStateProducts(newState)
        }

        setTimeout(() => {
          props.form.setFieldsValue({
            keys: nextKeys,
          })
        }, 300)
      },
      addAll: props => (params) => {
        const cobSelected = []; const keySelected = []; const idProduct = []; const products = []; const
          headBranch = []
        message.loading('Loading...')

        if (params) {
          props.selectBranchesOption.forEach((item, idx) => {
            cobSelected.push(item.id)
            keySelected.push(idx)
            headBranch.push(item.head_user ? item.head_user.nik : '')

            props.getBranches(`-perwakilan/${item.id}`).then(async (res) => {
              idProduct.push(res.map(data => data.id))
              products.push({
                list: res,
                load: false,
              })

              props.setStateBranches(products)
              await props.form.setFieldsValue({
                branch_perwakilan_ids: idProduct,
                keys: keySelected,
                branch_id: cobSelected,
                head_user: headBranch,
              })

              message.destroy()
            })
          })
        } else {
          props.selectCOBOption.forEach((item, idx) => {
            cobSelected.push(item.id)
            keySelected.push(idx)

            props.getDatas(
              { base: 'apiUser', url: `/all-products/with-head-user?keyword=&cob_id=${item.id}&product_type_id=`, method: 'get' },
            ).then(async (res) => {
              console.log(res.data)

              const oi = res.data.map(async(data => data.cob_id))

              console.log(oi)

              idProduct.push(res.data.map(data => data.id))
              headBranch.push(res.data.map(data => (data.head_user ? data.head_user.nik : '')))

              products.push({
                id: ['all'],
                list: res.data,
                load: false,
              })

              props.setStateProducts(products)
              await props.form.setFieldsValue({
                product_ids: idProduct,
                head_user: headBranch,
                keys: keySelected,
                class_of_business: cobSelected,
              })

              message.destroy()
            })
          })
        }
      },
      removeCard: props => (k) => {
        const {
          keys, class_of_business: cob, product_ids: productIds,
          branch_id: branchIds, branch_perwakilan_ids: perwakilanIds,
          role_group_id: group,
        } = props.form.getFieldsValue()
        if (keys.length === 1) return

        let newState
        if (group === 9) {
          newState = {
            branch_id: branchIds.filter((item, idx) => idx !== k),
            branch_perwakilan_ids: perwakilanIds.filter((item, idx) => idx !== k),
          }
        } else {
          newState = {
            class_of_business: cob.filter((item, idx) => idx !== k),
            product_ids: productIds.filter((item, idx) => idx !== k),
          }
        }
        props.form.setFieldsValue({
          ...newState,
          keys: keys.filter((key, idx) => idx !== k),
        })
        setTimeout(() => {
          if (group === 9) {
            props.setStateBranches((props.stateBranches || []).filter((item, idx) => idx !== k))
          } else {
            props.setStateProducts((props.stateProducts || []).filter((item, idx) => idx !== k))
          }
        }, 300)
      },
      handleSelect: props => (val, key) => {
        // Get role name for permission checkbox
        const dataRole = !isEmpty(props.selectGroupOption) ? props.selectGroupOption.filter(data => data.id === val) : []
        props.setRoleForCheckbox(first(dataRole))

        if (key === 'role-group') {
          props.setLoadRole(true)
          props.getRole(`?group=${val}`).then((res) => {
            props.setSelectOption(res)
            props.setLoadRole(false)
            props.form.setFieldsValue({
              role_id: undefined,
              head_user: '',
            })
          })
          if (val === 8) {
            props.getCOB().then((res) => {
              props.setSelectCOBOption(res)
              props.setLoadCOB(false)
              props.setStateProducts([{ load: false, list: [] }])
              props.setCurrentState({ ...props.currentState, keys: [0] })
            })
          } else if (val === 9) {
            props.getBranches('es/with-head-user').then((res) => {
              props.setSelectBranchesOption(res)
              props.setLoadBranches(false)
              props.setStateBranches([{ load: false, list: [] }])
              props.setCurrentState({ ...props.currentState, keys: [0] })
            })
          }
        }
        if (key === 'role') {
          currentId = 0
          props.setLoadCOB(false)
          props.setLoadBranches(false)
          props.setStateBranches([{ load: false, list: [] }])
          props.setStateProducts([{ load: false, list: [] }])
          props.setCurrentState({
            keys: [0],
            cob: [],
            productIds: [],
            branch: [],
            perwakilan: [],
          })
          props.form.setFieldsValue({
            keys: [0],
            class_of_business: [undefined],
            product_ids: [undefined],
            branch_id: [undefined],
            branch_perwakilan_ids: [undefined],
          })
        }
        if (key.includes('class_of_business_')) {
          const idx = Number(key.replace('class_of_business_', ''))
          const currentArr = props.stateProducts

          currentArr[idx].load = true
          currentArr[idx].id = val
          props.setStateProducts(currentArr[idx].list)

          props.getDatas(
            { base: 'apiUser', url: `/all-products/with-head-user?keyword=&cob_id=${key.includes('class_of_business_') ? val : ''}&product_type_id=${key.includes('product') ? val : ''}`, method: 'get' },
          ).then((res) => {
            currentArr[idx].load = false
            currentArr[idx].id = val
            currentArr[idx].list = (res.data || []).map(item => item)
            props.setStateProducts(currentArr)
          })
        }
        if (key.includes('branch_id_')) {
          const idx = Number(key.replace('branch_id_', ''))
          const currentArr = props.stateBranches
          currentArr[idx].load = true
          props.setStateBranches(currentArr)

          props.getBranches(`-perwakilan/${val}`).then((res) => {
            currentArr[idx].list = res
            currentArr[idx].load = false

            props.setStateBranches(currentArr)
          })
        }
      },
      handlePermission: props => (id) => {
        const {
          getDatas,
          setStatePermission,
          statePermission,
        } = props

        getDatas(
          { base: 'apiUser', url: `/permissions/all?type=role&type_id=${id}`, method: 'get' },
        ).then((res) => {
          setStatePermission({
            ...statePermission,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          setStatePermission({
            ...statePermission,
            load: false,
            list: [],
          })
        })
      },
      handleSubmit: props => (event) => {
        const { match, form, currentUser } = props
        event.preventDefault()

        form.validateFields((err, values) => {
          if (!err) {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)

            const { params } = match
            const payload = {
              nik: values.nik ? values.nik : '',
              name: values.name,
              email: values.email,
              role_id: values.role_id,
              permission_ids: values.permission_ids,
              phone_code_id: codeId[0].id,
              phone_number: values.phone_number,
              profile_pic_file: values.profile_pic_file ? props.avatarFileList : '',
            }

            const newArr = []
            if (values.role_group_id === 9) {
              values.branch_id.forEach((item, idx) => {
                let headBranch
                if (values.role_id === 32) {
                  const dataBranch = values.head_user.map(item => props.selectBranchesOption.filter(items => (items.head_user ? items.head_user.nik : '') === item))
                  dataBranch.map(item => item.map(items => items.head_user.id))
                  if (typeof values.head_user === 'string') {
                    headBranch = props.selectBranchesOption.filter(item => (item.head_user ? item.head_user.nik : '') === values.head_user)[0].head_user.id
                  } else {
                    headBranch = props.selectBranchesOption.filter(item => (item.head_user ? item.head_user.nik : '') === values.head_user[0])[0].head_user.id
                  }
                } else {
                  console.log(typeof values.head_user === 'string', typeof values.head_user )
                  if (typeof values.head_user === 'string') {
                    headBranch = props.selectBranchesOption.filter(item => (item.head_user ? item.head_user.nik : '') === values.head_user)[0].head_user.id
                  } else {
                    headBranch = props.selectBranchesOption.filter(item => (item.head_user ? item.head_user.nik : '') === values.head_user[0])[0].head_user.id
                  }
                }

                newArr.push({
                  id: item,
                  head_user_id: JSON.parse(JSON.stringify(headBranch)),
                  branch_perwakilan_ids: values.role_id === 32 ? [values.branch_perwakilan_ids[idx][0]] : values.branch_perwakilan_ids[idx],
                })
              })
              payload.branches = newArr
            }

            if (values.role_group_id === 8) {
              values.keys.forEach((key, idx) => {
                const headUser = values.head_user.map((item, indx) => props.stateProducts[idx].list.filter(items => items.id === indx))

                values.product_ids[idx].forEach((product) => {
                  const headUserId = headUser.filter((dataUser, index) => index === product)
                  newArr.push({
                    id: product,
                    head_user_id: !isEmpty(headUserId[0][0].head_user) ? headUserId[0][0].head_user.id : '',
                  })
                })
              })

              payload.products = newArr
            }

            props[`${params.id ? 'upd' : 'cre'}ateUser`](pickBy(payload, identity), (params.id || null))
              .then(() => {
                Swal.fire(`User has been ${params.id ? 'upd' : 'cre'}ated`, '', 'success')
                  .then(() => {
                    // if (params.id === currentUser.id) {
                    //   return props.clearCurrentUser()
                    // }
                    return history.push('/users')
                  })
              })
              .catch(error => message.error(error))
          }
        })
      },
      onCheckAllChange: props => (event) => {
        const permissionCheck = props.statePermission.list.map(perm => perm.id)
        props.setCheckedList(event.target.checked ? permissionCheck : [])
        props.setCheckAll(event.target.checked)
      },
      onChange: props => (list) => {
        props.setCheckedList(list)
      },
      getErrors: props => () => {
        let objError = {}

        Object.keys(props.errorObject).forEach((item) => {
          objError = {
            ...objError,
            [item]: {
              validateStatus: 'error',
              help: startCase((props.errorObject[item]).replace('_id', '').replace('_', ' ')),
            },
          }
          return props.setErrorsField(objError)
        })
      },
      handleProduct: props => (id, type) => {
        const cobSelected = []; const keySelected = []; const idProduct = []; const products = []; const headBranch = []

        if (type === 'branch') {
          id.forEach((item, idx) => {
            cobSelected.push(item.id)
            keySelected.push(idx)
            headBranch.push(item.head_user.nik)
            idProduct.push(item.branch_perwakilan.map(data => data.id))
            props.getBranches(`-perwakilan/${item.id}`).then(async (res) => {
              products.push({
                list: res,
                load: false,
              })

              props.setStateBranches(products)
              await props.form.setFieldsValue({
                branch_perwakilan_ids: idProduct,
                keys: keySelected,
                branch_id: cobSelected,
                head_user: headBranch,
              })
            })
          })
        } else {
          id.forEach((item, idx) => {
            cobSelected.push(item.id)
            keySelected.push(idx)

            props.getDatas(
              { base: 'apiUser', url: `/all-products/with-head-user?keyword=&cob_id=${item.id}&product_type_id=`, method: 'get' },
            ).then(async (res) => {
              idProduct.push(res.data.map(data => data.id))
              headBranch.push(res.data.map(data => (data.head_user ? data.head_user.nik : '')))

              products.push({
                id: ['all'],
                list: res.data,
                load: false,
              })

              props.setStateProducts(products)
              await props.form.setFieldsValue({
                product_ids: idProduct,
                head_user: headBranch,
                keys: keySelected,
                class_of_business: cobSelected,
              })
            })
          })
        }
      },
    }),
    lifecycle({
      async componentDidMount() {
        const {
          match, setSelectGroupOption,
          setLoadGroupRole, setIsEdit,
          stateCountry, setStateCountry,
          setProductType, productType,
          setCheckedList,
        } = this.props

        this.props.updateSiteConfiguration('breadList', ['Home', 'User - Form'])
        this.props.updateSiteConfiguration('activePage', 'users')
        this.props.userDetail({})
        this.props.getRole('-group?limit=1000').then((res) => {
          setSelectGroupOption(res)
          setLoadGroupRole(false)
        })
        if (match.params.id) {
          setIsEdit(true)
          this.props.setStateBranches([])
          this.props.fetchDetailUser(match.params.id).then((res) => {
            this.props.handlePermission(res.data.data.role.id)
            const permissionCheck = (res.data.data.permissions || []).map(perm => perm.id)
            setCheckedList(permissionCheck)
            if (!isEmpty(res.data.data.user_cobs)) {
              this.props.handleProduct(res.data.data.user_cobs, 'product')
            } else {
              this.props.handleProduct(res.data.data.branches, 'branch')
              this.props.getBranches('es/with-head-user').then((res) => {
                this.props.setSelectBranchesOption(res)
                this.props.setLoadBranches(false)
              })
            }
          })
        }

        this.props.getDatas(
          { base: 'apiUser', url: '/product-types/all', method: 'get' },
        ).then((res) => {
          setProductType({
            ...productType,
            options: res.data,
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })
      },
      componentDidUpdate(prevProps) {
        const {
          detailUser, setAvatarFileList, errorObject,
          setLoadRole, setSelectOption,
          setSelectCOBOption, setLoadCOB,
        } = this.props

        if (detailUser !== prevProps.detailUser) {
          setAvatarFileList(detailUser.profile_pic)
          if (detailUser.role) {
            const { group } = detailUser.role

            if (group.id === 8) {
              const { user_cobs: arrProducts } = detailUser

              this.props.getCOB().then((res) => {
                setSelectCOBOption(res)
                setLoadCOB(false)
              })
              if (!isEmpty(arrProducts)) {
                arrProducts.forEach(item => this.getDataProducts(item.id))
                this.props.form.setFieldsValue({
                  keys: arrProducts.map((item, idx) => idx),
                  class_of_business: arrProducts.map(item => item.id),
                  product_ids: arrProducts.map(item => item.product_type),
                })
              } else {
                this.props.form.setFieldsValue({
                  keys: [0],
                })
              }
            } else if (group.id === 9) {
              const { branches: arrBranches } = detailUser
              if (!isEmpty(arrBranches)) {
                arrBranches.forEach(item => this.getDataBranch(item.id))
                this.props.form.setFieldsValue({
                  keys: arrBranches.map((item, idx) => idx),
                  branch_id: arrBranches.map(item => item.id),
                  branch_perwakilan_ids: arrBranches.map(item => item.branch_perwakilan) || [],
                })
              } else {
                this.props.form.setFieldsValue({
                  keys: [0],
                })
              }
            }

            this.props.getRole(`?group=${group.id}`).then((res) => {
              setSelectOption(res)
              setLoadRole(false)
            })
          }
        }

        if (errorObject !== prevProps.errorObject) {
          this.props.getErrors()
        }
      },
      async getDataProducts(id) {
        const { stateProducts, setStateProducts } = this.props
        const newProducts = stateProducts

        await this.props.getProducts(`/${id}`).then((res) => {
          newProducts.push({ load: false, list: res, id })
        })

        setStateProducts(newProducts)
      },
      async getDataBranch(id) {
        const { stateBranches, setStateBranches } = this.props
        const newBranches = stateBranches

        await this.props.getBranches(`-perwakilan/${id}`).then((res) => {
          newBranches.push({ load: false, list: res, id })
        })

        setStateBranches(newBranches)
      },
    }),
  )(FormUserView),
)
