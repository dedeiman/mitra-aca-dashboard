import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchProduct, deleteProduct, productFailure } from 'actions/Product'
import qs from 'query-string'
import history from 'utils/history'
import ProductsView from 'components/pages/products'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataProduct,
    metaProduct,
  } = state.root.products

  return {
    isFetching,
    errorMessage,
    dataProduct,
    metaProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchProduct: bindActionCreators(fetchProduct, dispatch),
  deleteProduct: bindActionCreators(deleteProduct, dispatch),
  productFailure: bindActionCreators(productFailure, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('keyword', 'setKeyword', ''),
  withState('stateFilter', 'setFilter', {
    type: 'name',
    list: [],
  }),
  withState('currentPage', 'setCurrentPage', ''),
  withHandlers({
    handleFilter: props => () => {
      const payload = {
        keyword: props.keyword,
        page: props.currentPage,
        search_by: props.stateFilter.type,
      }
      history.push(`/products?${qs.stringify(payload)}`)
    },
  }),
  withHandlers({
    handleSearch: props => (value) => {
      const { type } = props.stateFilter
      props.setKeyword(value)
      setTimeout(() => {
        history.push(`/products${value ? `?search_by=${type}&keyword=${value}` : ''}`)
      }, 300)
    },
    handlePage: props => (page) => {
      props.setCurrentPage(page)
      setTimeout(() => {
        props.handleFilter()
      }, 300)
    },
    handleDelete: props => (id) => {
      props.deleteProduct(id).then(() => {
        Swal.fire({
          icon: 'success',
          title: 'Product has been deleted',
          showConfirmButton: false,
          timer: 1500,
        })
      })
    },
    closeError: props => () => props.productFailure(''),
  }),
  lifecycle({
    componentDidMount() {
      const {
        setKeyword, setCurrentPage, location, setFilter, stateFilter,
      } = this.props
      const search = qs.parse(location.search) || {}
      setCurrentPage(Number(search.page))
      setKeyword(search.keyword)
      this.props.updateSiteConfiguration('breadList', ['Home', 'Products'])
      this.props.updateSiteConfiguration('activePage', 'products')
      this.props.fetchProduct(location.search)
      setFilter({
        ...stateFilter,
        type: search.search_by || 'name',
      })
    },
  }),
)(ProductsView)
