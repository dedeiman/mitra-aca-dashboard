import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchRoleGroup, deleteRoleGroup, roleGroupFailure } from 'actions/RoleGroup'
import qs from 'query-string'
import history from 'utils/history'
import RoleGroupView from 'components/pages/roleGroup'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataRoleGroup,
    metaRoleGroup,
  } = state.root.roleGroup

  return {
    isFetching,
    errorMessage,
    dataRoleGroup,
    metaRoleGroup,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchRoleGroup: bindActionCreators(fetchRoleGroup, dispatch),
  deleteRoleGroup: bindActionCreators(deleteRoleGroup, dispatch),
  roleGroupFailure: bindActionCreators(roleGroupFailure, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('keyword', 'setKeyword', ''),
  withState('currentPage', 'setCurrentPage', ''),
  withHandlers({
    handleFilter: props => () => {
      const payload = {
        keyword: props.keyword,
        page: props.currentPage,
      }
      history.push(`/role-group?sort_by=name&order_by=asc&${qs.stringify(payload)}`)
    },
  }),
  withHandlers({
    handleSearch: props => (value) => {
      props.setKeyword(value)
      setTimeout(() => {
        history.push(`/role-group?sort_by=name&order_by=asc${value ? `&keyword=${value}` : ''}`)
      }, 300)
    },
    handlePage: props => (page) => {
      props.setCurrentPage(page)
      setTimeout(() => {
        props.handleFilter()
      }, 300)
    },
    handleDelete: props => (id) => {
      props.deleteRoleGroup(id).then(() => {
        Swal.fire({
          icon: 'success',
          title: 'Role Group has been deleted',
          showConfirmButton: false,
          timer: 1500,
        })
      })
    },
    closeError: props => () => props.roleGroupFailure(''),
  }),
  lifecycle({
    componentDidMount() {
      const { setKeyword, setCurrentPage, location } = this.props
      const search = qs.parse(location.search) || {}
      setCurrentPage(Number(search.page))
      setKeyword(search.keyword)
      this.props.updateSiteConfiguration('breadList', ['Home', 'Role Group'])
      this.props.updateSiteConfiguration('activePage', 'role-group')
      this.props.fetchRoleGroup(location.search)
    },
  }),
)(RoleGroupView)
