import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  fetchDetailRoleGroup, createRoleGroup, updateRoleGroup,
  roleGroupDetail,
} from 'actions/RoleGroup'
import {
  getDatas,
} from 'actions/Option'
import { Form } from 'antd'
import Swal from 'sweetalert2'
import FormRoleGroupView from 'components/pages/roleGroup/Form'
import history from 'utils/history'
import {
  pickBy, identity, startCase,
} from 'lodash'

export function mapStateToProps(state) {
  const { currentUser } = state.root.auth
  const {
    isFetching,
    errorMessage,
    errorObject,
  } = state.root.roleGroup
  return {
    currentUser,
    isFetching,
    errorMessage,
    errorObject,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailRoleGroup: bindActionCreators(fetchDetailRoleGroup, dispatch),
  createRoleGroup: bindActionCreators(createRoleGroup, dispatch),
  updateRoleGroup: bindActionCreators(updateRoleGroup, dispatch),
  roleGroupDetail: bindActionCreators(roleGroupDetail, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'roleGroupForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('isEdit', 'setIsEdit', false),
    withState('checkedList', 'setCheckedList', []),
    withState('checkAll', 'setCheckAll', false),
    withState('errorsField', 'setErrorsField', {}),
    withState('currentData', 'setCurrentData', {}),
    withState('statePermission', 'setStatePermission', {
      load: true,
      list: [],
    }),
    withHandlers({
      handleSubmit: props => (event) => {
        const {
          match, form,
        } = props
        event.preventDefault()

        form.validateFields((err, values) => {
          if (!err) {
            const { params } = match
            const payload = {
              code: values.code,
              name: values.name,
              permission_ids: values.permission_ids,
            }

            props[`${params.id ? 'upd' : 'cre'}ateRoleGroup`](pickBy(payload, identity), (params.id || null))
              .then(() => {
                Swal.fire(`Role Group has been ${params.id ? 'upd' : 'cre'}ated`, '', 'success')
                  .then(() => history.push('/role-group'))
              })
              .catch((error) => {
                if (error.errors && Object.keys(error.errors).length) {
                  let objError = {}
                  Object.keys(error.errors).forEach((item) => {
                    objError = {
                      ...objError,
                      [item]: {
                        validateStatus: 'error',
                        help: startCase((error.errors[item]).replace('_id', '').replace('_', ' ')),
                      },
                    }
                    props.setErrorsField(objError)
                  })
                }
              })
          }
        })
      },
      onCheckAllChange: props => (event) => {
        const permissionCheck = props.statePermission.list.map(perm => perm.id)
        props.setCheckedList(event.target.checked ? permissionCheck : [])
        props.setCheckAll(event.target.checked)
      },
      onChange: props => (list) => {
        props.setCheckedList(list)
      },
    }),
    lifecycle({
      async componentDidMount() {
        const {
          match, setIsEdit, setCurrentData, statePermission, setStatePermission, setCheckedList,
        } = this.props

        this.props.updateSiteConfiguration('breadList', ['Home', 'Role Group - Form'])
        this.props.updateSiteConfiguration('activePage', 'role-group')
        this.props.roleGroupDetail({})

        this.props.getDatas(
          { base: 'apiUser', url: '/permissions/all', method: 'get' },
        ).then((res) => {
          setStatePermission({
            ...statePermission,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          setStatePermission({
            ...statePermission,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          setIsEdit(true)

          this.props.fetchDetailRoleGroup(match.params.id).then((response) => {
            const { data } = response
            const permissionCheck = (data.data.permissions || []).map(perm => perm.id)
            setCheckedList(permissionCheck)
            setCurrentData(data.data)
          })
        }
      },
    }),
  )(FormRoleGroupView),
)
