import { connect } from 'react-redux'
import { compose, lifecycle, withState } from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import DashboardView from 'components/pages/dashboard'

export function mapStateToProps() {
  return {}
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateDashboard', 'setStateDashboard', {
    loadUser: true,
    loadProduct: true,
    totalUsers: [],
    totalProducts: [],
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('breadList', ['Home'])
      this.props.updateSiteConfiguration('activePage', 'dashboard')
      this.props.getDatas(
        { base: 'apiUser', url: '/admin/dashboard', method: 'get' },
      ).then((res) => {
        this.props.setStateDashboard({
          ...this.props.stateDashboard,
          loadUser: false,
          loadProduct: false,
          totalUsers: res.data.total_users,
          totalProducts: res.data.total_products,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateDashboard({
          ...this.props.stateDashboard,
          loadUser: false,
          loadProduct: false,
          totalUsers: [],
          totalProducts: [],
        })
      })
    },
  }),
)(DashboardView)
