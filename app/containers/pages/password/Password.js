import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, lifecycle, withState } from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import PasswordChange from 'components/pages/password/Password'

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withState('isSecondary', 'toggleSecondary', false),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'my-password')
    },
  }),
)(PasswordChange)
