import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withState, withHandlers,
} from 'recompose'
import Browser from 'utils/Browser'
import { resetPassword, verifyPassword, removeToken } from 'actions/Auth'
import { getDatas } from 'actions/Option'
import { Form, message } from 'antd'
import Swal from 'sweetalert2'
import SecondaryPasswordChange from 'components/pages/password/SecondaryPassword'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  return {
    currentUser,
  }
}
const mapDispatchToProps = dispatch => ({
  resetPassword: bindActionCreators(resetPassword, dispatch),
  verifyPassword: bindActionCreators(verifyPassword, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'changeSecondaryPassword' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('isLoading', 'setLoading', false),
    withState('fetchForgot', 'setFetching', false),
    withState('stateError', 'setStateError', {}),
    withState('isVisible', 'toggleVisible', false),
    withHandlers({
      sendEmail: props => () => {
        const payload = {
          identifier: props.currentUser.email,
          is_secondary: true,
          key: 'email',
        }
        props.setFetching(true)

        setTimeout(() => {
          props.getDatas(
            { base: 'apiUser', url: '/passwords/reset', method: 'post' },
            payload,
          ).then(() => {
            props.setFetching(false)
            Swal.fire('Link lupa second password telah dikirim', 'Silakan cek email anda', 'success')
              .then(() => {
                props.toggleVisible(false)
                removeToken()
                Browser.setWindowHref('/')
              })
          }).catch((err) => {
            props.setFetching(false)
            message.error(err.message)
          })
        }, 300)
      },
      onVerify: props => async (value) => {
        const { stateError, setStateError } = props

        setStateError({
          ...stateError,
          old_alternative_password: undefined,
        })

        await props.verifyPassword({ alternative_password: value }).then(() => {
          setStateError({
            ...stateError,
            old_alternative_password: undefined,
          })
        })
          .catch(() => {
            setStateError({
              ...stateError,
              old_alternative_password: { validateStatus: 'error', help: "Password is doesn't match" },
            })
          })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.setLoading(true)
        props.form.validateFields((err, values) => {
          if (err) {
            Swal.fire(
              'Terjadi kesalahan',
              'Mohon Periksa kembali data anda !',
              'error',
            )
          }

          if (!err) {
            props.resetPassword({ ...values, change_alternative_password: true }, true)
              .catch((error) => {
                props.setStateError({
                  ...props.stateError,
                  message: error,
                })
                Swal.fire(error, '', 'error')
                  .then(() => props.setLoading(false))
              })
          } else {
            props.setLoading(false)
          }

          props.setLoading(false)
        })
      },
    }),
  )(SecondaryPasswordChange),
)
