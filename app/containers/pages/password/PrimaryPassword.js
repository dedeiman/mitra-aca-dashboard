import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withState, withHandlers,
} from 'recompose'
import { resetPassword, verifyPassword } from 'actions/Auth'
import { Form } from 'antd'
import { isEmpty } from 'lodash'
import Swal from 'sweetalert2'
import PrimaryPasswordChange from 'components/pages/password/PrimaryPassword'

const mapDispatchToProps = dispatch => ({
  resetPassword: bindActionCreators(resetPassword, dispatch),
  verifyPassword: bindActionCreators(verifyPassword, dispatch),
})

export default Form.create({ name: 'changePrimaryPassword' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('isLoading', 'setLoading', false),
    withState('stateError', 'setStateError', {}),
    withHandlers({
      onVerify: props => async (value) => {
        const { stateError, setStateError } = props
        setStateError({
          ...stateError,
          old_password: undefined,
        })
        await props.verifyPassword({ password: value }).then(() => {
          setStateError({
            ...stateError,
            old_password: undefined,
          })
        })
          .catch(() => {
            setStateError({ ...stateError, old_password: { validateStatus: 'error', help: "Password is doesn't match" } })
          })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.setLoading(true)
        props.form.validateFields((err, values) => {
          if (err) {
            Swal.fire(
              'Terjadi kesalahan',
              'Mohon Periksa kembali data anda !',
              'error',
            )
          }

          if (!err && isEmpty(props.stateError.old_password)) {
            props.resetPassword({ ...values, change_password: true }, true)
              .catch((error) => {
                props.setStateError({
                  ...props.stateError,
                  message: error,
                })
                Swal.fire(error, '', 'error')
                  .then(() => props.setLoading(false))
              })
          } else {
            props.setLoading(false)
          }

          props.setLoading(false)
        })
      },
    }),
  )(PrimaryPasswordChange),
)
