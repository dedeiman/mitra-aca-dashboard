import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withHandlers, withState,
  withPropsOnChange,
} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { message, Form } from 'antd'
import { debounce } from 'lodash'
import Swal from 'sweetalert2'
import history from 'utils/history'
import FormPasswordView from 'components/pages/password'

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formPassword' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('errorsField', 'setErrorsField', {}),
    withState('isFetching', 'setLoading', false),
    withHandlers({
      onVerify: props => (value) => {
        const { errorsField, setErrorsField } = props
        props.getDatas(
          { base: 'apiUser', url: '/passwords/verify', method: 'post' },
          { password: value },
        ).catch(() => {
          setErrorsField({
            ...errorsField,
            old_password: { validateStatus: 'error', help: "Password is doesn't match" },
          })
        })
      },
      handleSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            const { setLoading } = props
            const payload = {
              ...values,
              change_password: true,
            }
            setLoading(true)
            props.getDatas(
              { base: 'apiUser', url: '/passwords/change', method: 'post' },
              payload,
            ).then(() => {
              setLoading(false)
              Swal.fire('Password has been change', '', 'success')
                .then(() => history.push('/dashboard'))
            }).catch((error) => {
              setLoading(false)
              message(error.message)
            })
          }
        })
      },
    }),
    withPropsOnChange(
      ['onVerify'],
      ({ onVerify }) => ({
        onVerify: debounce(onVerify, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        this.props.updateSiteConfiguration('activePage', 'my-password')
      },
    }),
  )(FormPasswordView),
)
