import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, lifecycle } from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import Password from 'components/pages/password'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const { currentUser: user } = state.root.auth
  const { currentUser } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  return {
    user,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'my-password')
    },
  }),
)(Password)
