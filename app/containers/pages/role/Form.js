import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  fetchDetailRole, createRole, updateRole,
  roleDetail,
} from 'actions/Role'
import {
  getDatas,
} from 'actions/Option'
import { Form } from 'antd'
import Swal from 'sweetalert2'
import FormRoleView from 'components/pages/role/Form'
import history from 'utils/history'
import {
  pickBy, identity, startCase,
} from 'lodash'

export function mapStateToProps(state) {
  const { currentUser } = state.root.auth
  const {
    isFetching,
    errorMessage,
    errorObject,
  } = state.root.role
  return {
    currentUser,
    isFetching,
    errorMessage,
    errorObject,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailRole: bindActionCreators(fetchDetailRole, dispatch),
  createRole: bindActionCreators(createRole, dispatch),
  updateRole: bindActionCreators(updateRole, dispatch),
  roleDetail: bindActionCreators(roleDetail, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'roleForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('isEdit', 'setIsEdit', false),
    withState('checkedList', 'setCheckedList', []),
    withState('checkAll', 'setCheckAll', false),
    withState('roleForCheckbox', 'setRoleForCheckbox', {}),
    withState('errorsField', 'setErrorsField', {}),
    withState('currentData', 'setCurrentData', {}),
    withState('statePermission', 'setStatePermission', {
      load: true,
      list: [],
    }),
    withState('stateGroup', 'setStateGroup', {
      load: true,
      list: [],
    }),
    withHandlers({
      handleSubmit: props => (event) => {
        const {
          match, form,
        } = props
        event.preventDefault()

        form.validateFields((err, values) => {
          if (!err) {
            const { params } = match
            const payload = {
              group_id: values.group_id,
              name: values.name,
              permission_ids: values.permission_ids,
            }

            props[`${params.id ? 'upd' : 'cre'}ateRole`](pickBy(payload, identity), (params.id || null))
              .then(() => {
                Swal.fire(`Role has been ${params.id ? 'upd' : 'cre'}ated`, '', 'success')
                  .then(() => history.push('/role'))
              })
              .catch((error) => {
                if (error.errors && Object.keys(error.errors).length) {
                  let objError = {}
                  Object.keys(error.errors).forEach((item) => {
                    objError = {
                      ...objError,
                      [item]: {
                        validateStatus: 'error',
                        help: startCase((error.errors[item]).replace('_id', '').replace('_', ' ')),
                      },
                    }
                    props.setErrorsField(objError)
                  })
                }
              })
          }
        })
      },
      handlePermission: props => (id) => {
        const {
          getDatas,
          setStatePermission,
          statePermission,
        } = props

        getDatas(
          { base: 'apiUser', url: `/permissions/all?type=group&type_id=${id}`, method: 'get' },
        ).then((res) => {
          setStatePermission({
            ...statePermission,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          setStatePermission({
            ...statePermission,
            load: false,
            list: [],
          })
        })
      },
      onCheckAllChange: props => (event) => {
        const permissionCheck = props.statePermission.list.map(perm => perm.id)
        props.setCheckedList(event.target.checked ? permissionCheck : [])
        props.setCheckAll(event.target.checked)
      },
      onChange: props => (list) => {
        props.setCheckedList(list)
      },
    }),
    lifecycle({
      async componentDidMount() {
        const {
          match, setIsEdit, setCurrentData, setCheckedList, stateGroup, setStateGroup,
        } = this.props

        this.props.updateSiteConfiguration('breadList', ['Home', 'Rol - Form'])
        this.props.updateSiteConfiguration('activePage', 'role')
        this.props.roleDetail({})

        this.props.getDatas(
          { base: 'apiUser', url: '/role-group', method: 'get' },
        ).then((res) => {
          setStateGroup({
            ...stateGroup,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          setStateGroup({
            ...stateGroup,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          setIsEdit(true)

          this.props.fetchDetailRole(match.params.id).then((response) => {
            this.props.handlePermission(response.data.data.group.id)
            const { data } = response
            const permissionCheck = (data.data.permissions || []).map(perm => perm.id)
            setCheckedList(permissionCheck)
            setCurrentData(data.data)
          })
        }
      },
    }),
  )(FormRoleView),
)
