const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const config = require('./base')

config.entry.app = [
  '@babel/polyfill',
  'webpack/webpack-public-path.js',
  'app.js',
]

config.plugins.push(
  new HtmlWebpackPlugin({
    template: 'app/assets/index-prod.html.ejs',
    inject: false,
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
)

config.devtool = 'source-map'

module.exports = config
